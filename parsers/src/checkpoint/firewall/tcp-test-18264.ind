#! META
name: chkp-tcp-test-18264
description: Test connectivity to management server over the CA port 18264.
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint
    linux-based: true
    role-firewall: true

#! COMMENTS
ca-accessible:
    why: |
        Devices that maintain VPN tunnels might authenticate using certificates, especially if both devices on either end of the tunnel are managed by the same management server. They would then need to connect to the management server to exchange certificates. If this communication is not working VPN tunnels could fail.
    how: |
        By checking the current connections on port 257 and then attempting to connect to the same IP on port 18264 the connection is verified.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The user must test connectivity manually to identify this issue.

#! REMOTE::SSH
${nice-path} -n 15 netstat -an | awk '{if ($NF == "ESTABLISHED" && match($(NF-2), ".*:18192$")) {split($(NF-1),splitArr,":"); print splitArr[1]}}' |uniq | while read ip ; do echo "echo > /dev/tcp/$ip/18264 && echo "$ip OK"" ; done | (while read cmd; do eval $cmd& sleep 0.1; CMD_PID=$! ; done ; sleep 20 ; kill $CMD_PID; )

#! PARSER::AWK

#10.3.3.75 OK
/ OK$/ {
	ip = $1
	caArr[ip] = 1
}

#-bash: /dev/tcp/10.3.3.75/18264: Connection refused
/tcp.*Connection refused/ {
	ip = $0
	
	#-bash: /dev/tcp/10.3.3.75/18264: Connection refused
	sub(/.*tcp\//, "", ip)
	
	#10.3.3.75/18264: Connection refused
	sub(/\/.*/, "", ip)

        #ignore 127.0.0.1 and 0.0.0.0
        if (length(ip) == 0 || ip == "127.0.0.1" || ip == "0.0.0.0") {
           next
        }
          
	caArr[ip] = 0
}


END {
	for (ip in caArr) {
		caTags["name"] = ip
		writeDoubleMetricWithLiveConfig("ca-accessible", caTags, "gauge", "3600", caArr[ip], "Certificate Authorities Accessible", "state", "name")
	}
}
