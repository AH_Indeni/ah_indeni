#! META
name: chkp-fw-tab-stats-novsx
description: Get firewall kernel table stats on non-vsx
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: "checkpoint"
    vsx:
        neq: "true"
    role-firewall: "true"

#! COMMENTS
kernel-table-actual:
    why: |
        The Check Point firewall utilizates kernel tables to store important operating information. Some of these tables have a limit. Reaching the limit could have undesirable results, including traffic loss and service disruption.
    how: |
        Retreive the current usage and limit from the kernel tables using the Check Point built-ind "fw tab" command.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Information regarding the kernel tables' utilization is only available through the command line.

kernel-table-limit:
    skip-documentation: true

#! REMOTE::SSH
didit=0; for inst in `${nice-path} -n 15 fw ctl multik stat | grep [0-9] | awk '{print $1}'`; do echo coreXL instance $inst; ${nice-path} -n 15 fw -i $inst tab | grep -B 1 " limit " && ${nice-path} -n 15 fw -i $inst tab -s; didit=1; done; if [ $didit -eq 0 ]; then echo coreXL instance 0; ${nice-path} -n 15 fw tab | grep -B 1 " limit " && ${nice-path} -n 15 fw tab -s; fi

#! PARSER::AWK

function send_metric() {

    for (tableName in currentUsage)
    {
        if (limits[tableName] != "")
        {
            if (tableName in accept)
            {
                if (tableName !~ /nrb_hitcount_table|cptls_server_cn_cache|fwx_alloc|fwx_cache|spii_multi_pset2kbuf_map|drop_tmpl_timer|fa_free_disk_space|mal_stat_src_week|rtm_view_[0-9]*/)
                {
                    tableTags["name"] = tableName
                    tableTags["corexl-id"] = coreXL_instance

                    writeDoubleMetric("kernel-table-actual", tableTags, "gauge", "300", currentUsage[tableName])
                    writeDoubleMetric("kernel-table-limit", tableTags, "gauge", "300", limits[tableName])
                }
            }
        }
    }
}

BEGIN {
    coreXL_str_found = 0
    tableName = "somethingwrong"
    coreXL_instance = "somethingwrong"

    accept["fwx_alloc"] = 1
    accept["connections"] = 1
    accept["fwx_cache"] = 1
    accept["cphwd_db"] = 1
    accept["cphwd_tmpl"] = 1
    accept["cphwd_dev_conn_table"] = 1
    accept["cphwd_vpndb"] = 1
    accept["cphwd_dev_identity_table"] = 1
    accept["cphwd_dev_revoked_ips_table"] = 1
    accept["cphwd_pslglue_conn_db"] = 1
    accept["f2f_addresses"] = 1
    accept["tcp_f2f_ports"] = 1
    accept["udp_f2f_ports"] = 1
    accept["tcp_f2f_conns"] = 1
    accept["udp_f2f_conns"] = 1
    accept["dos_suspected"] = 1
    accept["dos_penalty_box"] = 1
    accept["client_auth"] = 1
    accept["pdp_sessions"] = 1
    accept["pdp_super_sessions"] = 1
    accept["pdp_ip"] = 1
    accept["crypt_resolver_DB"] = 1
    accept["cluster_active_robo"] = 1
    accept["appi_connections"] = 1
    accept["cluster_connections_nat"] = 1
    accept["cryptlog_table"] = 1
    accept["DAG_ID_to_IP"] = 1
    accept["DAG_IP_to_ID"] = 1
    accept["decryption_pending"] = 1
    accept["encryption_requests"] = 1
    accept["IKE_SA_table"] = 1
    accept["ike2esp"] = 1
    accept["ike2peer"] = 1
    accept["inbound_SPI"] = 1
    accept["initial_contact_pending"] = 1
    accept["ipalloc_tab"] = 1
    accept["IPSEC_mtu_icmp"] = 1
    accept["IPSEC_mtu_icmp_wait"] = 1
    accept["L2TP_lookup"] = 1
    accept["L2TP_MSPI_cluster_update"] = 1
    accept["L2TP_sessions"] = 1
    accept["L2TP_tunnels"] = 1
    accept["MSPI_by_methods"] = 1
    accept["MSPI_cluster_map"] = 1
    accept["MSPI_cluster_update"] = 1
    accept["MSPI_cluster_reverse_map"] = 1
    accept["MSPI_req_connections"] = 1
    accept["MSPI_requests"] = 1
    accept["outbound_SPI"] = 1
    accept["peer2ike"] = 1
    accept["peers_count"] = 1
    accept["persistent_tunnels"] = 1
    accept["rdp_dont_trap"] = 1
    accept["rdp_table"] = 1
    accept["resolved_link"] = 1
    accept["Sep_my_IKE_packet"] = 1
    accept["SPI_requests"] = 1
    accept["udp_enc_cln_table"] = 1
    accept["udp_response_nat"] = 1
    accept["VIN_SA_to_delete"] = 1
    accept["vpn_active"] = 1
    accept["vpn_routing"] = 1
    accept["XPO_names"] = 1
    accept["vpn_queues"] = 1
    accept["ikev2_sas"] = 1
    accept["sam_requests"] = 1
    accept["tnlmon_life_sign"] = 1
    accept["cptls_server_cn_cache"] = 1
    accept["string_dictionary_table"] = 1
    accept["dns_cache_tbl"] = 1
    accept["nrb_hitcount_table"] = 1
}

#coreXL instance 0
/^coreXL instance/ {
    if (coreXL_str_found != 0) {
        send_metric()
    }
    coreXL_instance = $3
    coreXL_str_found = 1
    next
}

# -------- drop_tmpl_timer --------
/^--------/ {
    if (NF == 3) {
        tableName = $2
    }
    next
}

#dynamic, id 8172, attributes: keep, sync, expires 900, refresh, , hashsize 512, limit 25000
#dynamic, id 400, attributes: keep, sync, kbuf 1, expires never, limit 20000, hashsize 32768

/dynamic.*limit [0-9]/ {

    arrLen = split($0, arr, /(,\s|\s)/)
    
    # Loop through the array until the second to last item to avoid
    # stepping over the array element limit when using e + 1
    
    for(e = 1; e < arrLen; e++){
        if(arr[e] == "limit" && arr[e+1] ~ /^[0-9]+$/){
            limits[tableName] = arr[e+1]
            next            
        }
    }

    next

}

#localhost             vsx_firewalled                       0     1     1       0
/^localhost.*/ {

    tableName = $2
    currentValue = $4
    currentUsage[tableName] = currentValue

}

END {
    send_metric()
}

