#! META
name: cphaprob_list
description: Run "cphaprob list" to find pnotes in "problem"
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: "checkpoint"
    high-availability: "true"
    vsx:
        neq: "true"
    role-firewall: "true"

#! COMMENTS
clusterxl-pnote-state:
    why: |
        If a device in a cluster discovers a problem with itself, it will be because one pre-defined check "pnote" ("problem notification") has failed. It is interesting to know which pnote failed, to begin an investigation into why it happened.
    how: |
        By using the Check Point built-in command "cphaprob list" the pnotes are retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing pnotes is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 cphaprob -l list;${nice-path} -n 15 cphaprob list

#! PARSER::AWK

# cphaprob list only echoes the pnotes that has problems. This means that if there is a problem and it's solved indeni would
# alert like it should, but if the issue heals itself indeni would not not be able to auto-resolve the alert. 
# 
# To solve this we've added the -l switch to the command section. However, since -l is not available on SPLAT we need to run
# both "cphaprob list" and "cphaprob -l list" to account for both versions.
# As a consequence of that some information would be echoed twice on platforms supporting both commands.
# A verification has been added to avoid the same pnote state being written twice.

#Device Name: admin_down
/^Device Name:\s/ {
    deviceName = $0
    sub(/^Device Name:\s/, "", deviceName)
}

#Current state: problem (non-blocking)
#Current state: problem
/^Current state:\s/ {
	
    state = $3

	if (state == "problem") {
		status = 0 
	} else {
        status = 1
    }

    if (!(name in states)) {
        states[deviceName] = status
    }

}

END {

    for (deviceName in states) {
        
        tags["name"] = deviceName
        status = states[deviceName]

        writeDoubleMetricWithLiveConfig("clusterxl-pnote-state", tags, "gauge", "60", status, "ClusterXL Devices", "state", "name")

    }

}



