#! META
name: chkp-subscription-signatures-updates-vsx
description: Checks the status for blades that downloads signatures
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "checkpoint"
    vsx:  "true"
    or:
        -
            os.name: "gaia"
        -
            os.name: "secureplatform"
        -
            os.name: "gaia-embedded"
    role-firewall: "true"

#! COMMENTS
signature-update-status:
    why: |
        Several blades rely on signature updates to provide protection against the latest threats. If these updates are not working as expected, the gateway could miss new emerging threats.
    how: |
        The current update status for IPS, Anti-Bot, Anti-Virus, URL Filtering and Application control is retrieved.
    without-indeni: |
        An administrator could login and manually check this from the command line interface or from the Smart Dashboard.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface or Smart Dashboard.


#! REMOTE::SSH
${nice-path} -n 15 vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && fw vsx stat $id && (echo -n "blades: " && ${nice-path} -n 15 enabled_blades && echo -n "blades: " && ${nice-path} -n 15 enabled_blades.sh && ${nice-path} -n 15 date +"date: %Y-%m-%d %H:%M:%S" && echo "blade: AntiMalware" && ${nice-path} -n 15 cpstat -f update_status antimalware && echo "blade: Application-Control" && ${nice-path} -n 15 cpstat -f update_status appi && echo "blade: Url-Filtering" && ${nice-path} -n 15 cpstat -f update_status urlf && echo "blade: Anti-Spam" && ${nice-path} -n 15 avsu_client -app "KSS AV" get_version); done


#! PARSER::AWK

# Notes for Above SSH Command:
# 1) IPS signatures are downloaded on the management, and not checked in this script.
# 2) Runs "enabled_blades" command twice, since command can differ between versions.


# IMPORTANT NOTE: This file and subscription-signatures-updates-novsx.ind share duplicate code. If you make changes
# in this file, please check the other file to see if the same changes apply there.

BEGIN {
    vs_id = ""
    vs_name = ""
    blade_info_found = 0
}

function writeBladeStatus () {
    for (current_blade in blade_status_arr) {
        if (current_blade in enabled_blades) {
            # if the latest update was more than 14 days from now, consider the update not working.
            if ( (current_since_epoch - update_time_arr[current_blade]) > 1209600 ) {
                blade_status_arr[current_blade] = 0
            }
            tags["blade"] = current_blade
            tags["vs.id"] = vs_id
            tags["vs.name"] = vs_name
            writeDoubleMetric("signature-update-status", tags, "gauge", 0, blade_status_arr[current_blade])
        }
    }

    # Reset variables for the next run
    delete blade_status_arr
    delete update_time_arr
    delete enabled_blades
    delete features_arr
    delete tags
    blade_info_found = 0
    current_since_epoch = ""
    current_blade = ""
    vs_id = ""
    vs_name = ""

}

#VSID:            0
/VSID:/ {
    if (vs_id != "") {
        writeBladeStatus()
    }
    vs_id = $NF
}

#Name:            lab-CP-VSX1-R7730
/Name:/ {
    vs_name = $NF
}

#date: 2017-08-10 02:55:59
/^date: / {
    split($2, dateArr, "-")
    year = dateArr[1]
    month = dateArr[2]
    day = dateArr[3]

    split($3, clockArr, ":")
    hour = clockArr[1]
    minute = clockArr[2]
    second = clockArr[3]

    current_since_epoch = datetime(year, month, day, hour, minute, second)

    next
}

# Create an array with the currently enabled blades, to make sure to alert only for blades that are enabled.
#fw vpn urlf appi av ips identityServer anti_bot
/^blades: / {
    if (blade_info_found == 0 && $1 != "nice:") {
        features_arr_length = split($0, features_arr, " ")
        if (features_arr_length > 0)
            blade_info_found = 1

        for (id in features_arr) {
            # Translate name
            if (features_arr[id] == "urlf") {
                enabled_blades["Url-Filtering"] = ""

            } else if (features_arr[id] == "appi") {
                enabled_blades["Application-Control"] = ""

            } else if (features_arr[id] == "av") {
                enabled_blades["Anti-Virus"] = ""

            } else if (features_arr[id] == "ips") {
                enabled_blades["IPS"] = ""

            } else if (features_arr[id] == "anti_bot") {
                enabled_blades["Anti-Bot"] = ""

            } else if (features_arr[id] == "aspm") {
                enabled_blades["Anti-Spam"] = ""
            }
        }
    }

    next
}

#blade: AntiMalware
/^blade: / {
    current_blade = $2

    next
}

#AB Update status:           up-to-date
#Update status:           up-to-date
#Update status:           failed
#AV Update status:           failed
#AB Update status:
/Update status:/ {
    # Both Anti-Bot and Anti-Virus are bundled in "AntiMalware", so we need to create more specific names for the
    # current_blade here.
    if ($1 == "AB") {
        current_blade = "Anti-Bot"
    } else if ($1 == "AV") {
        current_blade = "Anti-Virus"
    }

    if ($NF == "up-to-date" || $NF == "new") {
        blade_status_arr[current_blade] = 1
    } else {
        blade_status_arr[current_blade] = 0
    }

    next
}

#DB version:              17081003
#AV DB version:              1708100834
/DB version: / {
    # Both Anti-Bot and Anti-Virus are bundled in "AntiMalware", so we need to create more specific names for the
    # current_blade here.
    if ($1 == "AB") {
        current_blade = "Anti-Bot"
    } else if ($1 == "AV") {
        current_blade = "Anti-Virus"
    }

    # Make sure that there is a date to process, otherwise set epoch time to 0
    if ($NF ~ /[0-9]/) {
        yymmdd = $NF
        year = substr(yymmdd, 1, 2)
        year = "20" year
        month = substr(yymmdd, 3, 2)
        day = substr(yymmdd, 5, 2)

        update_time_arr[current_blade] = date(year, month, day)
    } else {
        update_time_arr[current_blade] = 0
    }

    next
}


#Result: sig version=1010101, sig date=1247058927 (Wed Jul  8 06:15:27 2009
/Result: sig version/ {
    if ($NF ~ /[0-9]/) {
        year = $NF
        month = $(NF-3)
        month = parseMonthThreeLetter(month)
        day = $(NF-2)

        update_time_arr[current_blade] = date(year, month, day)
    } else {
        update_time_arr[current_blade] = 0
    }

    # Setting status to 1, if the update time is more than 2 weeks old, it will be set to 0 in the END section
    blade_status_arr[current_blade] = 1

    next
}

END {
    # Write data for last virtual guest
    writeBladeStatus()
}