#! META
name: chkp-mgmt-cpmistat
description: Show redundant mgmt sync status for non MDS management server.
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
    role-management: true
    mds:
        neq: true

#! COMMENTS
mgmt-ha-sync-state:
    why: |
        When an high availability management server is set up, it is important for it to be in sync with the master. Otherwise the unsynced data could get lost if the active management server suddenly goes down.
    how: |
        By using the Check Point built-in "cpmistat" command, the sync status is retrieved.
    without-indeni: |
        An administrator could login and manually run the command, or check SmartDashboard.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The easiest way to check the status is in SmartDashboard.
mgmt-ha-sync-state-description:
    why: |
        The sync state description gives details on why there is an issue with the current sync state.
    how: |
        By using the Check Point built-in "cpmistat" command, the sync status is retrieved.
    without-indeni: |
        An administrator could login and manually run the command, or check SmartDashboard.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The easiest way to check the status is in SmartDashboard.

#! REMOTE::SSH
${nice-path} -n 15 cat $FWDIR/database/netobj_objects.C | ${nice-path} -n 15 awk '/^\t: \(.+$/ { if (host && mg == 2 ) { gsub(/\t\: \(/,"",host); print  "cpmistat -o schema -r mg " host } mg = 0; host=$0 } /:primary_management \(false\)/ { mg++ } /:management \(true\)/ { mg++ } END { if (host && mg == 2 ) { gsub(/\t\: \(/,"",host); print "cpmistat -o schema -r mg " host} }' | (while read cmd ; do { echo "Process \"$cmd\" started"; ($cmd && echo $cmd) & sleep 0.1 ; pid=$(ps aux|grep "$cmd" |grep -v "grep" |awk '{print $2}') ;  PID_LIST+=" $pid"; } done ; sleep 20; for id in $PID_LIST ; do  kill $id; done) ; sleep 1; echo


#! PARSER::AWK

############
# Script explanation: This script is a bit complicated. Here is how it works
# Step 1: Get a list of all secondary management servers. 
#		Parse netobj_objects.C and find all hosts that have the following two properties:
#		:primary_management (false)
#		:management (true)
# Step 2: For each of the devices found in step 1, run cpmistat command against them.
# Step 3: To be able to query as many devices as possible, and have everything ready within the 30s ind script timeout, run cpmistat in parallel (since it has a 35s timeout), and record the PID of each command so we can kill them all after 20s.
###########


# Process "cpmistat -o schema -r mg HA-Management2" started
/^Process \"cpmistat -o schema -r mg/ {
	# We have attempted to connect to this host
	gsub(/\"/,"",$7)
	statusArr[$7] = 0
}

# :mgStatusOK (0)
# :mgStatusOK (1)
/:mgStatusOK/ {
	# We got some result from the attempt, but we dont know from who
	status = $2
	gsub(/^\(/,"",status)
	gsub(/\)$/,"",status)
}

# :mgSyncStatus (Lagging)
# :mgSyncStatus (Synchronized)
# :mgSyncStatus ("N/R (Self synchronization is not relevant)")
/:mgSyncStatus/ {
	# We got some result from the attempt, but we dont know from who
	
	split($0,splitArr,"mgSyncStatus")
	statusMessage = trim(splitArr[2])
	gsub(/^\(/,"",statusMessage)
	gsub(/\)$/,"",statusMessage)
	gsub(/\"/,"",statusMessage)
}


# cpmistat -o schema -r mg lab-CP-MGMT1-2
/^cpmistat -o schema -r mg/ {
	# We now know from who the above result was from

	statusMessageArr[$6] = statusMessage
	statusArr[$6] = status
}


END {
	# Write metrics
	for (id in statusArr) {
		t["name"] = id
		if (statusMessageArr[id] != "N/R (Self synchronization is not relevant)") {
			if (statusMessageArr[id] != "") {
				writeComplexMetricString("mgmt-ha-sync-state-description", t, statusMessageArr[id])
			} else {
				writeComplexMetricString("mgmt-ha-sync-state-description", t, "No message (Timeout)")
			}
		}
		if (statusArr[id] != "") {
			writeDoubleMetric("mgmt-ha-sync-state", t, "gauge", 300, statusArr[id])
		}
	}
}
