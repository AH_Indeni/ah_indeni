#! META
name: chkp-mdsstat-interrogation-R80
description: Check if this is an MDS or CMA server
type: interrogation
requires:
    vendor: "checkpoint"
    or:
        -
            os.name: "gaia"
        -
            os.name: "secureplatform"
    role-firewall:
        neq: "true"
    or:
        -
            os.version: "R80.10"
        -
            os.version: "R80.20"

#! REMOTE::SSH
${nice-path} -n 15 mdsstat

#! PARSER::AWK

############
# Why: Check if the device is running MDS(Provider-1)
# How: Check if command "mdsstat" gives output that indicates an MDS environment.
###########

# When reading this script, keep in mind that whether we're interrogating the actual MDS host or one of the CMAs, the
# "SSH context" is the same. I.e., when the SSH command executes, it doesn't (can't) know whether it is hitting
# the actual host IP or one of the CMA IPs. So, during interrogation, we don't actually make a distinction between them:
# whether it's the host or a CMA, we run exactly the same script (this one).


#| MDS |  -  | 10.10.6.13      | up 15549   | up 15547 | up 15544 | up 15638 |
#|MDS|  -  | 10.10.6.13      | up 15549   | up 15547 | up 15544 | up 15638 |
# DON'T MATCH:
#| CMA |MDS_Indeni1_Server     | 192.168.194.52  | up 6483    | up 6450  | up 6413  | up 10054 |
/^\|\s*MDS/ {
    # Whether this is CMA or actual MDS host, it's a "management server"
    writeTag("role-management", "true")
    # Whether this is CMA or actual MDS host, we tag it as "mds"
    writeTag("mds", "true")
    # MDS is a virtual system (CMAs) like VSX
    writeTag("vsx", "true")

    is_mds = 1
}

#| CMA |lab-CP-MGMT-R7730_Management_Server                                                  | 10.10.6.10      | up 26197   | up 26156 | up 26088 | up 26345 |
/CMA.*[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ {
    vs_count++
}

END {
    # don't write these tags for a non-MDS environment
    if (is_mds) {
        writeTag("vs-count", vs_count)
        writeTag("license-vs-ratio", "1")
    }
}