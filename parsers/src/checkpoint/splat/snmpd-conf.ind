#! META
name: chkp-secureplatform-snmpd-conf
description: displays SNMP information
type: monitoring
monitoring_interval: 60 minute
requires:
    vendor: checkpoint
    os.name: secureplatform

#! COMMENTS
snmp-enabled:
    skip-documentation: true

snmp-version:
    skip-documentation: true

snmp-contact:
    why: |
        If the wrong contact is specified in the SNMP settings, the network monitoring team might not have the information they need to notify the administrator when needed.
    how: |
        Parse the /etc/snmp/snmpd.conf file and retreive the current configuration for SNMP.
    without-indeni: |
        An administrator could log in and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface.

snmp-location:
    why: |
        The SNMP location should be set correctly, since it gives the administrator a fast and easy way to determine where the device is located.
    how: |
        Parse the /etc/snmp/snmpd.conf file and retreive the currently configuration for SNMP.
    without-indeni: |
        An administrator could log in and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface.

snmp-communities:
    why: |
        If the default SNMP communities are configured, like "public" or "private" it could allow unauthorized clients to poll the device.
    how: |
        Parse the /etc/snmp/snmpd.conf file and retreive the currently configuration for SNMP.
    without-indeni: |
        An administrator could log in and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface.

unencrypted-snmp-configured:
    why: |
        If SNMP is not using encryption to retrieve data from devices, the data could be intercepted. Authentication data such as passwords used to login and get the data could also be compromised.
    how: |
        Parse the /etc/snmp/snmpd.conf file and retreive the currently configuration for SNMP.
    without-indeni: |
        An administrator could log in and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SNMP information is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 snmp service stat && cat /etc/snmp/snmpd.conf

#! PARSER::AWK

BEGIN {
	snmpUnencrypted = "false"
	snmpVersion = ""
}

# SNMP service disabled.
# SNMP service enabled and listening on port 161.
/^SNMP service / {
	status = $3
	if (status == "enabled") {
		writeComplexMetricString("snmp-enabled", null, "true")
	} else if (status == "disabled.") {
		writeComplexMetricString("snmp-enabled", null, "false")
	}
}



# syslocation  "secret Lab"
/^syslocation|^sysLocation/ {
	snmpLocation = $0
	gsub(/syslocation|sysLocation/, "", snmpLocation)
	gsub(/\"/, "", snmpLocation)
}

#syscontact MrAdmin
/^syscontact|^sysContact/ {
	snmpContact = $0
	gsub(/syscontact|sysContact/, "", snmpContact)
	gsub(/\"/, "", snmpContact)
}

/^rocommunity |rwcommunity / {
	icommunity++
	community = $2
	communities[icommunity, "community"] = community
	snmpUnencrypted = "true"
	if (snmpVersion != "" && snmpVersion != "v2") {
		snmpVersion = snmpVersion "/v2"
	} else {
		snmpVersion = "v2"
	}
	
	if ($1 == "rwcommunity") {
		communities[icommunity, "permissions"] = "read-write"
	} else if ($1 == "rocommunity") {
		communities[icommunity, "permissions"] = "read-only"
	}
}

/^rouser |^rwuser / {
	user = $2
	security = $3
	
	snmpuser[user, "username"] = user
	snmpuser[user, "security"] = security
	
	if (snmpVersion != "" && snmpVersion != "v3") {
		snmpVersion = snmpVersion "/v3"
	} else {
		snmpVersion = "v3"
	}
	
	if ($1 == "rouser") {
		snmpuser[user, "permissions"] = "ReadOnly"
	} else if ($1 == "rwuser") {
		snmpuser[user, "permissions"] = "ReadWrite"
	}

}

END {
	if (status == "enabled") {
		writeComplexMetricObjectArray("snmp-communities", null, communities)
		writeComplexMetricStringWithLiveConfig("snmp-contact", null, trim(tolower(snmpContact)), "SNMP - Contact")
		writeComplexMetricStringWithLiveConfig("snmp-location", null, trim(snmpLocation), "SNMP - Location")
		writeComplexMetricString("unencrypted-snmp-configured", null, snmpUnencrypted)
		writeComplexMetricString("snmp-version", null, snmpVersion)
	}
}
