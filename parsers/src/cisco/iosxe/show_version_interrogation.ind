#! META
name: iosxe-show-version-interrogation
description: IOS-XE show version
type: interrogation

#! REMOTE::SSH
show version

#! PARSER::AWK

# Extract 'version' and 'product' (router)
#Cisco IOS XE Software, Version 03.16.02.S - Extended Support Release
#Cisco IOS XE Software, Version 16.03.06
/^Cisco IOS XE Software, Version/ {

    isIOSXE = 1

    # retrieve version
    split($0, array_header, ",")
    split(trim(array_header[2]), array_version, " ")
    version = trim(array_version[2])

    # Only 'router' have this type of line
    product = "router"
}

# Extract 'version' and 'product' (switch)
#Cisco IOS Software, IOS-XE Software, Catalyst 4500 L3 Switch  Software (cat4500e-UNIVERSALK9-M), Version 03.07.03.E RELEASE SOFTWARE (fc3)
#Cisco IOS Software, IOS-XE Software, Catalyst 4500 L3 Switch  Software (cat4500es8-UNIVERSAL-M), Version 03.08.05.E RELEASE SOFTWARE (fc2)
/^Cisco IOS Software, IOS-XE Software,.*, Version/{
    isIOSXE = 1

    array_header_size = split($0, array_header, ",")
    split(trim(array_header[array_header_size]), array_version, " ")
    version = trim(array_version[2])

    # Only 'switches' have this type of line.
    # Use this check just as extra check.
    if ($0 ~ /(Switch)|(Catalyst)/i) {
        product = "switch"
    } else {
        product = "router"
    }


}


#Cisco CISCO2901/K9 (revision 1.0) with 479232K/45056K bytes of memory.
#Cisco 886VAG2 (MPC8300) processor (revision 1.0) with 472064K/52224K bytes of memory.
#Cisco C881-K9 (revision 1.0) with 488524K/35763K bytes of memory.
#cisco WS-C2960-24PC-L (PowerPC405) processor (revision L0) with 65536K bytes of memory.
/^[C|c]isco.*(processor|revis).*memory/ {
    model = trim($2)
}

#Processor board ID FGL210610F0
/^Processor board ID/ {
    serial = trim($4)
}

#DEVICE-NAME uptime is 8 weeks, 15 hours, 32 minutes
#chickenhawk.indeni.com uptime is 8 weeks, 15 hours, 32 minutes
/uptime is/ {
    # Read the hostname (first field)
    split($1, hostNameArr, /\./)
    hostname = hostNameArr[1]
}

END {
    #Only publish tags if IOS-XE
    if (isIOSXE == 1) {
        writeTag("vendor", "cisco")
        writeTag("os.name", "ios-xe")


        if (product != "") {
            writeTag("product", product)
        }

        if (version != "") {
            writeTag("os.version", version)
        }

        if (serial != "") {
            writeTag("serial", serial)
        }

        if (model != "") {
            writeTag("model", model)
        }

        if (hostname != "") {
            writeTag("hostname", hostname)
        }
    }
}