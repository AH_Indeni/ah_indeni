#! META
name: nexus-show-logging-info
description: Nexus show logging info
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "cisco"
    os.name: "nxos"

#! COMMENTS
logging-console-status:
    why: |
        Event logging gives you visibility into the operation of a Cisco NX-OS device and the network in which it is deployed. Cisco NX-OS provides several flexible logging options that can help achieve the network management and visibility goals of an organization. With Cisco NX-OS, it is possible to send log messages to monitor sessions, or to the console. However, doing so can elevate the CPU load of a Cisco NX-OS device, and therefore is not recommended. Furthermore, you are advised to send logging information to the local log buffer or the local log file, which can be viewed using the show logging command.
    how: |
        This script logins into the Cisco Nexus switch using SSH and retrieves the output of the "show logging info | i ogging" command. The output of this command shows the logging configuration/features status.
    without-indeni: |
        It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

logging-monitor-status:
    why: |
        Event logging gives you visibility into the operation of a Cisco NX-OS device and the network in which it is deployed. Cisco NX-OS provides several flexible logging options that can help achieve the network management and visibility goals of an organization. With Cisco NX-OS, it is possible to send log messages to monitor sessions, or to the console. However, doing so can elevate the CPU load of a Cisco NX-OS device, and therefore is not recommended. Furthermore, you are advised to send logging information to the local log buffer or the local log file, which can be viewed using the show logging command.
    how: |
        This script logins into the Cisco Nexus switch using SSH and retrieves the output of the "show logging info | i ogging" command. The output of this command shows the logging configuration/features status.
    without-indeni: |
        It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

logging-timestamp-status:
    why: |
        Event logging gives you visibility into the operation of a Cisco NX-OS device and the network in which it is deployed. The configuration of logging time stamps helps to correlate events across network devices. It is important to implement a correct and consistent logging time-stamp configuration to help ensure that you can correlate logging data. Logging time stamps should be configured to include millisecond precision.
    how: |
        This script logins into the Cisco Nexus switch using SSH and retrieves the output of the "show logging info | i ogging" command. The output of this command shows the logging configuration/features status.
    without-indeni: |
        It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

logging-source-interface-status:
    why: |
        To provide an increased level of consistency when collecting and reviewing log messages, you should statically configure a logging source interface. Accomplished through the logging source-interface interface command, statically configuring a logging source interface helps ensure that the same IP address appears in all logging messages that are sent from an individual Cisco NX-OS device.
    how: |
        This script logins into the Cisco Nexus switch using SSH and retrieves the output of the "show logging info | i ogging" command. The output of this command shows the logging configuration/features status.
    without-indeni: |
        It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

logging-logfile-status:
    why: |
        Cisco NX-OS software supports the use of a local log buffer in the form of a log file so that an administrator can view locally generated log messages. The use of buffered logging to the log file is highly recommended instead of logging to either the console or monitor sessions.
    how: |
        This script logins into the Cisco Nexus switch using SSH and retrieves the output of the "show logging info | i ogging" command. The output of this command shows the logging configuration/features status.
    without-indeni: |
        It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

logging-server-status:
    why: |
        You should send logging information to a remote syslog server. By doing so, you can correlate and audit network and security events across network devices more effectively. Note that syslog messages are transmitted unreliably by UDP and in cleartext. For this reason, any protections that a network offers to management traffic (for example, encryption and out-of-band access) should be extended to include syslog traffic.
    how: |
        This script logins into the Cisco Nexus switch using SSH and retrieves the output of the "show logging info | i ogging" command. The output of this command shows the logging configuration/features status.
    without-indeni: |
        It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

logging-con-mon-severity-level-status:
    why: |
        Each internal system software component of Cisco NX-OS that is capable of logging using the syslog facility can be assigned one of eight severity levels that range from level 0, Emergencies, through level 7, Debug. The severity level chosen will determine the level, granularity, and frequency of messages generated for that component. Unless specifically required, you are advised to avoid logging at level 7. Logging at level 7 produces an elevated CPU load on the device that can lead to device and network instability.
    how: |
        This script logins into the Cisco Nexus switch using SSH and retrieves the output of the "show logging info | i ogging" command. The output of this command shows the logging configuration/features status.
    without-indeni: |
        It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show logging info | i ogging

#! PARSER::AWK

# A helper function that publish a metric-double status type in live config
# Please note that the tag:
# 'name-ui' will be displayed in live-config ui
# 'name' will be displayed in the alert ui (if any)
function publishMetricStatus(name, value, ui_category, ui_title, tag_name) {
    if (value == 0 || value == 1) {
        tags["name-ui"] = ui_title
        tags["name"] = tag_name
        writeDoubleMetricWithLiveConfig(name, tags, "gauge", 300, value, ui_category, "state", "name-ui");
    }
}
# If 'value' is 1 then return 'enabled' otherwise 'disabled'
function getEnabledDisabledFromValue(value) {
    return value == 1 ? "enabled" : "disabled";
}

# Return 1 if the 'text' has the 'textToFind'
function hasText(text, textToFind) {
   index_match = match(tolower(text), textToFind);
   return index_match > 0
}


BEGIN {
    status_logging_console = -1;
    status_logging_monitor = -1;
    status_logging_timestamp_in_secs = -1;
    status_logging_src_if = -1;
    status_logging_logfile = -1;
    status_logging_server = -1;

    status_logging_co_mon = -1;
    is_logging_monitor_console = -1
    is_logging_monitor_debug = -1

    logging_time_units = "";
}

#Logging console:                enabled (Severity: critical)
/Logging console:/{
    status_logging_console = hasText($0, "enabled");
    is_logging_console_debug = hasText($0, "debugging");
}

#Logging monitor:                enabled (Severity: debugging)
/Logging monitor:/{
    status_logging_monitor = hasText($0, "enabled");
    is_logging_monitor_debug = hasText($0, "debugging");
}

#Logging timestamp:              MicroSeconds
/Logging timestamp:/{
    status_logging_timestamp_in_secs = hasText($0, " seconds");
    logging_time_units = $NF;
}

#Logging source-interface :      disabled
/Logging source-interface/{
    status_logging_src_if = hasText($0, "enabled");
}

#Logging server:                 disabled
/Logging server:/{
    status_logging_server = hasText($0, "enabled");
}

#Logging logfile:                enabled
/Logging logfile:/{
    status_logging_logfile = hasText($0, "enabled");
}


END {

    # Publish metrics only if we have a valid value ( value != -1 )
    publishMetricStatus("logging-console-status", status_logging_console, "Logging Configuration", "Logging-Console-Status", "Logging console is " getEnabledDisabledFromValue(status_logging_console))
    publishMetricStatus("logging-monitor-status", status_logging_monitor, "Logging Configuration", "Logging-Monitor-Status", "Logging monitor is " getEnabledDisabledFromValue(status_logging_monitor))
    publishMetricStatus("logging-timestamp-status", status_logging_timestamp_in_secs, "Logging Configuration", "Logging-Timestamp-seconds-Status", "Logging timestamp is " logging_time_units)
    publishMetricStatus("logging-source-interface-status", status_logging_src_if, "Logging Configuration", "Logging-Source-Interface-Status", "Logging source-interface is " getEnabledDisabledFromValue(status_logging_src_if))
    publishMetricStatus("logging-logfile-status", status_logging_logfile, "Logging Configuration", "Logging-Logfile-Status", "Logging logfile is " getEnabledDisabledFromValue(status_logging_logfile))
    publishMetricStatus("logging-server-status", status_logging_server, "Logging Configuration", "Logging-to-Remote-Server-Status", "Logging remote server is " getEnabledDisabledFromValue(status_logging_server))

    # This metric valie is '1' If Console or Monitor level is 'debugging' otherwise the values is '0'
    if (is_logging_console_debug != -1 || is_logging_monitor_debug != -1) {
        is_debug = (is_logging_monitor_debug || is_logging_console_debug)
        publishMetricStatus("logging-con-mon-severity-level-status", is_debug, "Logging Configuration", "Logging-Console/Monitor-Debugging-level-status", "Logging Severity level for console/monitor is" (is_debug ? " " : " not ")  "debugging")
    }

}
