#! META
name: nexus-show-copp-status
description: Nexus show copp status
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "cisco"
    os.name: "nxos"

#! COMMENTS
copp-status:
    why: |
        The CoPP is critical to network operation. It controls the rate at which packets are allowed to reach the Supervisor. The CoPP feature can also be used to restrict IP packets that are destined for the infrastructure device itself and require control-plane CPU processing. This script checks if Control Plane Policy (CoPP) has been activated to the Nexus which is recommended by Cisco.
    how: |
        This script logins into the Cisco Nexus switch using SSH and retrieves the output of the "show copp status" command. The output of this command shows the status of this feature.
    without-indeni: |
        It is not possible to poll this data through SNMP or Syslog.
    can-with-snmp: false
    can-with-syslog: false


#! REMOTE::SSH
show copp status

#! PARSER::AWK
BEGIN {
    policy_map = "";
}

/Policy-map attached to the control-plane/ {
    policy_map = $NF
}

END {
   tags["name"] = "Control Plane (CoPP)"
   is_policy_map_none = (policy_map != "None")
   writeDoubleMetricWithLiveConfig("copp-status", tags, "gauge", 300, is_policy_map_none, "Policy Map Status", "state", "name")
}