 #! META
name: radware-ssh-stats-slb-pip
description: Measure number of ports used by Proxy IP against number of ports free
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: "radware"
    os.name: "alteon-os"
    or:
        -
            vadc: "true"
        -
            standalone: "true"

radware-lb-snatpool-usage:
    why: |
        PIP port exhaustion could be a problem in environments that have large amounts of connections and too few source Proxy IP's. If all available port combinations are exhausted it will lead to connections being dropped by the system.
    how: |
        This alert uses cli command "/stats/slb/pip/" to extract the real time number of current server side connections per SNAT pool. PIPs are allocated to specific ports and VLANs, so association of virtual servers is dependant on ports and VLANs designated
    without-indeni: |
        An adminstrator could login to the device through SSH, execute the command "/stats/slb/pip/". Then for each Proxy IP address and look at "current used". More information can be found here: https://support.radware.com/app/answers/answer_view/a_id/15691/~/max-number-of-active-sessions-per-alteon-pip-address.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: | 
        Can be done with Management GUI.
radware-lb-snatpool-limit:
    why: |
        PIP pool limit is not easy to track with Radware. Port exhaustion could be a problem in environments that have large amounts of connections and too few source Proxy IPs. If all available port combinations are exhausted it will lead to connections being dropped by the system.
    how: |
        This alert uses cli command "/stats/slb/pip/" to extract the total available allocation for each PIP pool. PIPs are allocated to specific ports and VLANs, so association of virtual servers is dependant on ports and VLANs designated
    without-indeni: |
        An adminstrator could login to the device through SSH, execute the command "/stats/slb/pip/". Then for each Proxy IP address and look at "current used". More information can be found here: https://support.radware.com/app/answers/answer_view/a_id/15691/~/max-number-of-active-sessions-per-alteon-pip-address.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: | 
        Can be done with Management GUI.

#! REMOTE::SSH
/stats/slb/pip/ / /

#! PARSER::AWK

#10.10.10.10                              2031584        0          0
$1 ~ /[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/ {
    proxyIp = $1
    freeCount = $2
    usedCount = $3
    totalCount = freeCount + usedCount

    metricTags["name"] = proxyIp
    writeDoubleMetric("radware-lb-snatpool-limit", metricTags, "gauge", 60, totalCount)
    writeDoubleMetric("radware-lb-snatpool-usage", metricTags, "gauge", 60, usedCount) 
}
