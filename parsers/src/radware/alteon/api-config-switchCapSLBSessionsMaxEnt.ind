#! META
name: radware-api-config-switchCapSLBSessionsMaxEnt
description: get the maximum number of SLB sessions supported
type: monitoring
monitoring_interval: 59 minute 
requires:
    os.name: "alteon-os"
    vendor: "radware"
    or:
        -
            vsx: "true"
        -
            standalone: "true"

#! COMMENTS
concurrent-connections-limit:
    why: |
        It is important to track the limit of possible concurrent SLB sessions that are made with the ADC. This is a separate limitation from PIP utilization and should be tracked as well..
    how: |
        This script runs the "/config/switchCapSLBSessionsMaxEnt" through the Alteon API gateway.
    without-indeni: |
        An administrator would need to log in to the device and run a CLI command or run the API command "/config/switchCapSLBSessionsMaxEnt".
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        Can be done through Management GUI (Vision or Alteon VX).

#! REMOTE::HTTP
url: /config/switchCapSLBSessionsMaxEnt
protocol: HTTPS

#! PARSER::JSON
_metrics:
    -
        _value.double:
            _value: switchCapSLBSessionsMaxEnt
        _tags:
            "im.name":
                _constant: "concurrent-connections-limit"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "SLB Sessions - Capacity"
            "im.dstype.displayType":
                _constant: "number"
