#! META
name: linux-lspci
description: list hardware
type: monitoring
monitoring_interval: 5 minutes
requires:
    linux-based: "true"

#! COMMENTS
interface-hw:
    why: |
        While administrating a wide range of hardware devices, it is important to know what hardware is used. For example, network interface cards.
    how: |
        Using the built in "lspci" command, the hardware of the network interfaces cards is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the hardware for the network interface cards is only available from the command line.

#! REMOTE::SSH
${nice-path} -n 15 lspci

#! PARSER::AWK

/Ethernet/ {
	ieth++
	split($0,ethernetArr,":")

	eth[ieth, "interface-model"]=ethernetArr[3]
}

END {
	writeComplexMetricObjectArray("interface-hw", null, eth)
}
