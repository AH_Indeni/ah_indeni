#! META
name: linux-proc-uptime
description: Record uptime in milliseconds
type: monitoring
monitoring_interval: 5 minutes
requires:
    or:
        -
            linux-based: "true"
        -
            linux-busybox: "true"

#! COMMENTS
uptime-milliseconds:
    why: |
        If uptime is suddently reduced, this means the device has rebooted.
    how: |
        The current uptime is retreived from /proc/uptime.
    without-indeni: |
        An administrator could login and manually check the uptime.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or vendor-provided management interface.

#! REMOTE::SSH
${nice-path} -n 15 cat /proc/uptime

#! PARSER::AWK

############
# Script explanation: /proc/uptime is used instead of the command "uptime" since /proc/uptime has a higher granularity, since it shows uptime in seconds.
############

#896218.37 3217298.93
/^[0-9]/ {
	uptime = $1 * 1000
	writeDoubleMetricWithLiveConfig("uptime-milliseconds", null, "gauge", 300, uptime, "Uptime", "duration", "")
}
