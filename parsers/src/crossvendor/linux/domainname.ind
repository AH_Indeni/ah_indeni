#! META
name: linux-domainname
description: records the domain name for the system
type: monitoring
monitoring_interval: 60 minutes
requires:
    linux-based: "true"
    vendor:
        neq: f5

#! COMMENTS
domain:
    why: |
        An incorrect domain name could mean that some DNS names cannot be resolved.
    how: |
        The domain name is retrieved by running the "domainname" command.
    without-indeni: |
        An administrator could check this via the management interface or command line interface.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface or management interface.	

#! REMOTE::SSH
${nice-path} -n 15 domainname

#! PARSER::AWK

#test.local
/^[a-zA-Z0-9]/ {
	domainname = $1
}

END {
	writeComplexMetricStringWithLiveConfig("domain", null, domainname, "Domain Name")
}
