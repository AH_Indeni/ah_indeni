#! META
name: f5-route
description: Get connected networks
type: monitoring
monitoring_interval: 30 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
connected-networks-table:
    why: |
       Capture the networks that are directly connected to the device.
    how: |
       This script logs into the F5 unit through SSH and retrieves the output of the "route -n" command.
    without-indeni: |
       An administrator could log into the device through SSH and execute the command "route -n" to see the networks connected via the VLAN interfaces.
    can-with-snmp: false
    can-with-syslog: false
    
#! REMOTE::SSH
route -n

#! PARSER::AWK

BEGIN {

    iConnectedNetworks = 0
    
	netMaskToCIDR["0.0.0.0"] = 0
	netMaskToCIDR["128.0.0.0"] = 1
	netMaskToCIDR["192.0.0.0"] = 2
	netMaskToCIDR["224.0.0.0"] = 3
	netMaskToCIDR["240.0.0.0"] = 4
	netMaskToCIDR["248.0.0.0"] = 5
	netMaskToCIDR["252.0.0.0"] = 6
	netMaskToCIDR["254.0.0.0"] = 7
	netMaskToCIDR["255.0.0.0"] = 8
	netMaskToCIDR["255.128.0.0"] = 9
	netMaskToCIDR["255.192.0.0"] = 10
	netMaskToCIDR["255.224.0.0"] = 11
	netMaskToCIDR["255.240.0.0"] = 12
	netMaskToCIDR["255.248.0.0"] = 13
	netMaskToCIDR["255.252.0.0"] = 14
	netMaskToCIDR["255.254.0.0"] = 15
	netMaskToCIDR["255.255.0.0"] = 16
	netMaskToCIDR["255.255.128.0"] = 17
	netMaskToCIDR["255.255.192.0"] = 18
	netMaskToCIDR["255.255.224.0"] = 19
	netMaskToCIDR["255.255.240.0"] = 20
	netMaskToCIDR["255.255.248.0"] = 21
	netMaskToCIDR["255.255.252.0"] = 22
	netMaskToCIDR["255.255.254.0"] = 23
	netMaskToCIDR["255.255.255.0"] = 24
	netMaskToCIDR["255.255.255.128"] = 25
	netMaskToCIDR["255.255.255.192"] = 26
	netMaskToCIDR["255.255.255.224"] = 27
	netMaskToCIDR["255.255.255.240"] = 28
	netMaskToCIDR["255.255.255.248"] = 29
	netMaskToCIDR["255.255.255.252"] = 30
	netMaskToCIDR["255.255.255.254"] = 31
	netMaskToCIDR["255.255.255.255"] = 32
}

#10.245.0.0      0.0.0.0         255.255.255.0   U     0      0        0 EXTERNAL
/^[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}/{

    # We don't want
    # Routes using a gateway: Flag = "G"
    # Routes for a host: Flag = "H"
    # Routes created by a redirect: Flag = "D"
    # Routes modified by a redirect: Flag = "M"
    
    if(! match($1, /^127\./) && ! match($4, /(G|H|D|M)/)){
        iConnectedNetworks++
        connectedNetworks[iConnectedNetworks, "network"] = $1
        connectedNetworks[iConnectedNetworks, "mask"] = netMaskToCIDR[$3]
    }
}

END {

    #Value: [ {
    #  "network" : "10.245.0.0",
    #  "mask" : "24"
    #}, {
    #  "mask" : "24",
    #  "network" : "10.245.50.0"
    #}, {
    #  "network" : "192.168.197.0",
    #  "mask" : "24"
    #} ]

    writeComplexMetricObjectArray("connected-networks-table", null, connectedNetworks)
}
