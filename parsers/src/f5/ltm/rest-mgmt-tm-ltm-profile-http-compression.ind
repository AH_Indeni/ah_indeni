 #! META
name: f5-rest-mgmt-tm-ltm-profile-http-compression
description: Determine compression level of compression profiles
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"

#! COMMENTS
f5-high-compression-profile-high-gzip-level:
    why: |
        Setting a higher compression level makes compressed content smaller, but the cost of higher CPU usage and longer time to compress the content. The difference in terms of percentage gets lower the higher the level and setting this too high is not recommended. This metric would alert for any levels above 6.
    how: |
        This alert uses the iControl REST interface to extract compression levels set for each of the configured HTTP Compression Profiles.
    without-indeni: |
        Login to the device's web interface and click on "Local Traffic" -> "Profiles" -> "HTTP Compression". This would show a list of the available compression profiles. For each profile in the list, check the option "gzip Compression Level". In case the configuration is divided in multiple partitions changing to the "All [Read-only]" partition is recommended.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/ltm/profile/http-compression?$select=fullPath,gzipLevel,contentTypeInclude
protocol: HTTPS

#! PARSER::JSON

_metrics:
    - # Record compression profiles with compression level lower or equal to 6
        _groups:
            "$.items[0:]":
                _tags:
                    "im.name":
                        _constant: "f5-high-compression-profile-high-gzip-level"
                    "name":
                        _value: "fullPath"
                    "im.dstype.displaytype":
                        _constant: "boolean"
                _value.complex:
                    value:
                        _value: "gzipLevel"
