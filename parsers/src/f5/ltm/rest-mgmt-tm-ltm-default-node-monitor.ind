 #! META
name: f5-rest-mgmt-tm-ltm-default-node-monitor
description: Determine node default monitors
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"

#! COMMENTS
f5-default-node-monitor-configured:
    why: |
        It is good practice to have a basic check for node monitors as it's easier to fast establish correlations between multiple failing members during an outage.
    how: |
        This alert uses the iControl REST interface to extract the default node monitors.
    without-indeni: |
        Login to the device's web interface and click on "Local Traffic" -> "Nodes" -> "Default Monitor". This would show the default monitor configuration for nodes.
    can-with-snmp: fase
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/ltm/default-node-monitor
protocol: HTTPS

#! PARSER::JSON

_metrics:

    - # Check if a default monitor has been configured
        _tags:
            "im.name":
                _constant: "f5-default-node-monitor-configured"
            "im.dstype.displaytype":
                _constant: "boolean"
        _temp:
            "monitorCount":
                _count: "$.rule"
        _transform:
            _value.complex:
                value: |
                    {
                        if(temp("monitorCount") > 0){
                            print "true"
                        } else {
                            print "false"
                        }
                    }
