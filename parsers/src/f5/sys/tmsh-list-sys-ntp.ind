#! META
name: f5-tmsh-list-sys-ntp
description: Get the configured NTP servers and timezone
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
ntp-servers:
    why: |
        Not having an NTP server configured could make the clock slowly drift, which makes log entries and other information harder to summarize between devices. If the clock drifts very far out, there could also be issues with validating certificates.
    how: |
        Indeni logs in over SSH and executes "tmsh -q list sys ntp". The output is then parsed for any ntp server configuration.
    without-indeni: |
        An administrator could login to the unit through SSH, enter TMSH and issue the command "list sys ntp" to see the configured NTP servers. This information is also availble through the Web Interface by navigating to "System" -> "Configuration" -> "Device" -> "NTP".
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This information is available via both TMSH and the Web Interface.
timezone:
    why: |
         A correct time and time zone is very important for many reasons. An incorrectly configured time zone could mean that timestamps on logs are incorrect. Indeni will identify when two devices are part of a cluster and alert if the timezone setting is different.
    how: |
        Indeni logs in over SSH and executes "tmsh -q list sys ntp". The output is then parsed for the configured timezone.
    without-indeni: |
        An administrator could login to the unit through SSH, enter TMSH and issue the command "list sys ntp" to see the configured timezone. This information is also availble through the Web Interface by navigating to "System" -> "Platform".
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This information is available via both TMSH and the Web Interface.  

#! REMOTE::SSH
tmsh -q list sys ntp

#! PARSER::AWK

BEGIN {
    timeZone = ""
}

#    servers { 192.168.70.1 pool.ntp.org }
/^\s+servers/ {

    configurationLine = $0

    #    servers { 192.168.70.1 pool.ntp.org }
    sub(/^\s+servers\s\{\s/, "", configurationLine )
    
    #192.168.70.1 pool.ntp.org }
    sub(/\s\}/, "", configurationLine)
    
    #192.168.70.1 pool.ntp.org
    split(configurationLine, ntpServerArr, /\s/)

    #["192.168.70.1", "pool.ntp.org"]
    for (iNTP in ntpServerArr) {
        ntpServers[iNTP, "ipaddress"] = ntpServerArr[iNTP]
        ntpServers[iNTP, "version"] = 4
        ntpServers[iNTP, "type"] = "N/A"
    }

}

#    timezone Europe/Stockholm
/^\s+timezone/ {

    timeZone = $2

}

END {

    #If America/Los Angeles is specified as time zone the tmsh command would not list any timezone.
    #Thus we need to set this manually if the timeZone variable is empty.
    if (timeZone == "") {
        timeZone = "America/Los Angeles"
    }
    
    writeComplexMetricStringWithLiveConfig("timezone", null, timeZone, "Timezone")
    writeComplexMetricObjectArrayWithLiveConfig("ntp-servers", null, ntpServers, "NTP Servers Defined")
    
}