#! META
name: f5-rest-mgmt-tm-sys-cpu-stats
description: Determine cpu usage
type: monitoring
monitoring_interval: 1 minute
includes_resource_data: true
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"

#! COMMENTS
cpu-usage:
    why: |
        Tracking the CPU utilization of all processing cores on an F5 unit is critical to ensure capacity isn't reached. Should capacity be reached on data plane cores, new connections and sessions may fail to be processed. A high CPU utilization on the management plane may result in issues in managing the device, or updating its settings.
    how: |
        This alert uses the F5 iControl REST API to retrieve the current CPU usage of all cores.
    without-indeni: |
        The utilization of the data plane and management plane CPUs is available by logging into the web interface and clicking on "Statistics" -> "Performance". In the statistics the CPU details can be accessed by clicking on "View Detailed Graph..." in the section called "System CPU Usage". This information is also available by logging into the device through SSH, entering TMSH and issuing the command "show sys cpu".
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/sys/cpu/stats
protocol: HTTPS

#! PARSER::JSON

_metrics:
    -
        _groups:
            "$.entries.*.nestedStats.entries.*.nestedStats.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "cpu-usage"
                    "im.dstype.displaytype":
                        _constant: "percentage"
                    "resource-metric":
                        _constant: "true"
                    "cpu-id":
                        _value: "cpuId.value"
                    "cpu-is-avg":
                        _constant: "false"
                _value.double:
                    _value: "usageRatio.value"
