#! META
name: bluecoat-show-resources
description: Fetch cpu usage stats
type: monitoring
monitoring_interval: 1 minute
includes_resource_data: true
requires:
    vendor: "bluecoat"
    os.name: "sgos"

#! COMMENTS
memory-total-kbytes:
    skip-documentation: true

memory-free-kbytes:
    skip-documentation: true

memory-usage:
    why: |
        Monitoring memory usage is critical for evaluate the system's health and make sure it's not over utilized.
        High memory usage could lead to production impact in terms of dropped packets.
    how: |
       This script logs into the Bluecoat Proxy using SSH and retrieves the memory utilization from the "show resources" command.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show resources

#! PARSER::AWK

BEGIN {
    tags["name"] = "RAM"
}

function convertToKB(memBytes){

    #4,105,961,472
    gsub(/,/, "", memBytes)
    totalMemKbytes = memBytes/1000

    return totalMemKbytes
}

#  Total memory available                      :            4,105,961,472 bytes
/^\s+Total memory available/{
    totalMemKbytes = convertToKB($(NF-1))
    writeDoubleMetricWithLiveConfig("memory-total-kbytes", tags, "gauge", "60", totalMemKbytes, "Memory - Total", "kilobytes", "name")
}

#    Total memory free                         :            2,780,155,904 bytes
/^\s+Total memory free/{
    totalMemKbytesFree = convertToKB($(NF-1))
    writeDoubleMetricWithLiveConfig("memory-free-kbytes", tags, "gauge", "300", totalMemKbytesFree, "Memory - Free", "kilobytes", "name")
}

#    Committed memory:(% of total available)   :                       24 %
/^\s+Committed memory:\(%/{
    tags["resource-metric"] = "true"
    memoryUsage = $(NF-1)
    writeDoubleMetricWithLiveConfig("memory-usage", tags, "gauge", "60", memoryUsage, "Memory - Usage", "percentage", "name")
}
