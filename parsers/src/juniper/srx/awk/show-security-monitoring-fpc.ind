#! META
name: junos-show-security-monitoring-fpc
description: Retrieve the number of connections per second for last 96 seconds 
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
connections-per-second:
    why: |
        connections per second
    how: |
        This script retrieves the average connections per second for the last 96 seconds on fpc 0 by running  the "show security monitoring fpc X" command. 
    without-indeni: |
        An administrator could log on to the device to run the command "show security monitoring fpc 0" to collect the same information.
    can-with-snmp: false 
    can-with-syslog: false
    vendor-provided-management: |
        The commamnd line is available to retrieve this information

#! REMOTE::SSH
show chassis hardware node local | match node
show security monitoring fpc 0 

#! PARSER::AWK
BEGIN {
    node0 = 0
    node1 = 0
    cluster = 0 
    node_tag["name"] = "standalone"
}
 
#node0:
/^(node0)/{
   node0++
   cluster = 1
   if (node0 == 2) {
       node_tag["name"] = "node0"
   }
}

#node1:
/^(node1)/{
   node1++
   if (node0 == 2) {
       node0 = 1
   } else {
       node_tag["name"] = "node1"
   }
}

#IPv4  Session Creation Per Second (for last 96 seconds on average):  128
/^(IPv4\s+Session\s+Creation\s+Per\s+Second)/{
    if (cluster == 0 || node0 == 2 || node1 == 2) {
        CPS = $NF 
        writeDoubleMetric("connections-per-second", node_tag, "gauge", 60, CPS)
    }
}
