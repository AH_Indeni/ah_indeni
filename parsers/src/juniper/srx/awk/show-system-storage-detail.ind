#! META
name: junos-show-system-storage-detail
description: JUNOS retrieve the storage status
type: monitoring
monitoring_interval: 10 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
disk-usage-percentage:
    why: |
        It is very important to monitor the disk space usage of a system. If the disk space is full it will prevent writing more data to the disk. Compressing and moving data from a disk that is 100% full is time consuming, which is why it is important to take care of any such issue early.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show system storage detail" command. The output includes the device's storage utilization.
    without-indeni: |
        An administrator could login and manually list the disk space usage. Vendors generally provide tools which provide access to this information.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is accessible from the command line interface or vendor-provided tools, as well as SNMP.

disk-used-kbytes:
    why: |
        Used to display how much, in kilobytes, of the partition being used. If the file system gets data that should be written to disk can be lost.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show system storage detail" command. The output includes the device's storage utilization.
    without-indeni: |
        An administrator could login and manually list the disk space usage. Vendors generally provide tools which provide access to this information.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is accessible from the command line interface or vendor-provided tools, as well as SNMP.

disk-total-kbytes:
    why: |
        Used to display the total partition size, in kilobytes.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show system storage detail" command. The output includes the device's storage utilization.
    without-indeni: |
        An administrator could login and manually list the disk space usage. Vendors generally provide tools which provide access to this information.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is accessible from the command line interface or vendor-provided tools, as well as SNMP.

#! REMOTE::SSH
show chassis hardware node local | match node
show system storage detail

#! PARSER::AWK
BEGIN {
    node0 = 0
    node1 = 0
    cluster = 0
    # List of uninteresting mount points for JUNOS specifically:
    uninterestingmounts["/dev"] = "true"
    uninterestingmounts["/jail/dev"] = "true"
    uninterestingmounts["/junos"] = "true"
    uninterestingmounts["/junos/cf/dev"] = "true"
    uninterestingmounts["/junos/dev/"] = "true"
    uninterestingmounts["/proc"] = "true"
}

#node0:
/^node0/ {
    node0++ 
    cluster = 1
}

#node1:
/^node1/ {
    node1++
    cluster = 1
    if (node0 == 2) {
        node0 = 1
    }
}

#/dev/sda1  295561     24017    256284   9% /boot
/(\d+)%/ {
    mount = trim($NF)

    if (cluster == 0 || node0 == 2 || node1 == 2) {
        if (!uninterestingmounts[mount]) {
            usage = $(NF-1)
            sub(/%/, "", usage)
            available = $(NF-2)
            used = $(NF-3)
    	    total = $(NF-4)

            mounttags["file-system"] = mount

            writeDoubleMetricWithLiveConfig("disk-usage-percentage", mounttags, "gauge", "60", usage, "Mount Points - Usage", "percentage", "file-system")
            writeDoubleMetricWithLiveConfig("disk-used-kbytes", mounttags, "gauge", "60", used, "Mount Points - Used", "kbytes", "file-system")
            writeDoubleMetricWithLiveConfig("disk-total-kbytes", mounttags, "gauge", "60", total, "Mount Points - Total", "kbytes", "file-system")
        }  
    }
}
