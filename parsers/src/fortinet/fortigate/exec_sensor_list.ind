#! META
name: fortios-exec-sensor-list
description: Fortinet Firewall retrieves tempetature sensors list
type: monitoring
monitoring_interval: 20 minutes
requires:
    vendor: fortinet
    os.name: FortiOS
    product: firewall
    vdom_enabled: false
    vdom_root: true

# --------------------------------------------------------------------------------------------------
# The script publish the following metrics
#
# [hardware-element-status]           [0|1 for CPU/FAN/PS hardware elements]
# --------------------------------------------------------------------------------------------------


#! COMMENTS
hardware-element-status:
    why: |
       It checks the sensors and readings of every hardware component (i.e. Temperature, Power Supply Status and Fan
       Status). In particular, it gets the alarm status from the "exec sensor list" command output (when flag is set
       to 0, the component is working correctly and when flag is set to 1, the component is not working properly). Only
       the fortinet firewall models with temperature sensors will provide this information. More details can be found
       to at: http://kb.fortinet.com/kb/viewContent.do?externalId=FD36793&sliceId=1
    how: |
       This script logs in to the Fortinet Firewall and retrieves the output of the "exec sensor list" command. The
       output includes a table with info about the temperature, fan and power supply status.
    without-indeni: |
       An administrator would need to manually log into the device and run the FortiOS command "exec sensor list" and
       review pertinent information.
    can-with-snmp: true
    can-with-syslog: false


#! REMOTE::SSH
exec sensor list

#! PARSER::AWK
BEGIN{
    # Initialize the table index
    table_hardware_index = 0
}

# Parse all the needed info and store them in the table
# 2 +5V               alarm=0  value=5.048  threshold_status=0
# 4 CPU VCCP          alarm=0  value=0.9609  threshold_status=0
#13 DDR3 VTT          alarm=0  value=0.736  threshold_status=0
#24 FAN_TMP_3         alarm=0  value=42  threshold_status=0
/alarm=.*value=.*threshold_status=/ {

    indexNameStart = index($0, $2)
    indexNameEnd = index($0, "alarm=")
    nameOfElement = trim(substr($0, indexNameStart, indexNameEnd-indexNameStart))

    # Add only 'FAN', 'CPU', 'PS' hardware elements
    if (nameOfElement ~ /(CPU)|(FAN)|(PS)/){
        table_hardware_index++


        # Parsing the text after 'alarm=', in order to read the value (0|1) of alarm
        alarmOfElement = trim(substr($0,indexNameEnd+6,1))
        statusOfElement = 1
        if (alarmOfElement == 1) {
          statusOfElement = 0
        }

        table_hardware[table_hardware_index, "name"] = nameOfElement
        table_hardware[table_hardware_index, "status"] = statusOfElement
    }
}

END {
    # Publishing table data (no Category / Default)
    for(index_of_table = 1; index_of_table <= table_hardware_index ; index_of_table++) {
         tags_to_publish["name"] = table_hardware[index_of_table, "name"]
         writeDoubleMetricWithLiveConfig("hardware-element-status", tags_to_publish, "gauge", 300, table_hardware[index_of_table, "status"], table_hardware[index_of_table, "name"], "state", "name")
    }
}



