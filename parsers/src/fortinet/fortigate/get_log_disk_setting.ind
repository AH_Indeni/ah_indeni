#! META
name: fortios-get-log-disk-setting
description: FortiGate check disk logging status
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: fortinet
    os.name: FortiOS
    product: firewall
    vdom_enabled: false
    vdom_root: true

# --------------------------------------------------------------------------------------------------
# The script publish the following metrics
#
# [fortios-local-disk-logging]           [0|1, 1 if enable ]
# --------------------------------------------------------------------------------------------------


#! COMMENTS
fortios-local-disk-logging:
    why: |
        This metric is used to identify if logging is enabled to the local disk. If the FortiGate unit contains a Hard
        Disk (HD) then it can be configured to store logs there. Enabling logging to the local disk is not recommended
        since it may saturate the hardware resources of the device. Besides, based on the network security best
        practice is recommended to store logs to a remote device. Fortinet recommends uploading the logs for analysis
        to a remote device such as  FortiAnalyzer or FortiGuard Analysis server. Check the link below for more
        information:
        https://docs.fortinet.com/uploaded/files/3421/logging-reporting-54.pdf
    how: |
        This script logs into the FortiGate using SSH and retrieves the log disk status by using the output of the
        FortiOS command "get log disk setting". The "get log disk setting" command provides detailed information about
        the configured logging disk settings.
    without-indeni: |
        The user would have to login to the device and use the "get log disk setting" command to identify if the
        logging is enabled on the disk.
    can-with-snmp: false
    can-with-syslog: false


#! REMOTE::SSH
get log disk setting

#! PARSER::AWK


# Parse the status metric (enable or disable)
#status              : disable
#status              : enable
/^status /{

    # Reading the last part of the sentence (disable | enable)
    status_value = tolower($NF)

    # Compare the value with 'enable'
    is_status_enable = 0
    if (status_value == "enable") {
        is_status_enable = 1
    }
}

END {
    # Publishing in category 'Logging Local Service'
    # Please note that IF the 'is_status_enable' is not set/initialized then the metric will not be published.
    tags["name"] = "Logging Hard Disk"
    writeDoubleMetricWithLiveConfig("fortios-local-disk-logging", tags, "gauge", 60, is_status_enable, "Logging Local Service", "boolean", "name")
}



