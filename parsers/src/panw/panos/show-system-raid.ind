#! META
name: panos-show-system-raid
description: Show RAID and disk status and configuration
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
raid-status: |
    why: |
        It is important that we know when a RAID array may be failing due to disk failures and identify which disk is failed.
    how: |
        This script will check for each RAID array and report the status of each disk in the array.
    without-indeni: |
        It appears that SNMP polling in combination with syslog is the only way to see what drives are installed and when one has failed.
    can-with-snmp: false
    can-with-syslog: true

panw-raid-configured: |
    why: |
        It is important to know that a device may have been ordered without redundant disks in the array. It is possible especially on a pa-5000 series firewall to order a firewall with only one disk. This is certainly not recommended if you do not have high-availability configured with another firewall. It will still show that you have a RAID because it uses a RAID configuration although you may only have one disk as a member of that array. 
    how: |
        This script will check for the RAID array in scenarios where only one RAID array exists by looking at devices that have Overall RAID status. From there it looks to see if one of the disks in that array report as missing. That indicates RAID is not truly enabled to protect your device from downtime due to drive failures.
    without-indeni: |
        On boot, a PA-5000 series firewall will warn you of a disk missing in the system log and would be sent in a syslog event or as an email alert. Most syslog solutions will not alert you of a missing disk so you would have to manually define an alert.
can-with-snmp: false

can-with-syslog: true

#! REMOTE::SSH
show system raid

#! PARSER::AWK

BEGIN {
    first = 1
    raid_status = 1
    missing_disk = 0
    overall_raid_status_found = 0

}

#Disk Pair A
/^Disk Pair/{

    if (!first) {
        raid_states[raid_name] = raid_status
    }

    # Reset variables
    raid_status = 1
    raid_name = "Disk Pair " $3
    first = 0
}

# This catches the instances where disk pairs is not used and sets the name to "raid"
#Overall RAID status                        Good
/^Overall RAID status/ {
    overall_raid_status_found = 1
    raid_name = "Single RAID"
}


#   Disk id 1                            Present
#   Disk id 2                            Missing
/^\s+Disk id/ {

    if ($NF != "Missing" && $NF != "Present") {
        raid_status = 0
    }

    # Begin test for raid not enabled. See examples above for disk and raid status input.

    if ($NF == "Missing" && overall_raid_status_found) {
        missing_disk++
    }

    if (!disk_states[raid_name]) {
        disk_states[raid_name] = $3 ":" $NF
    } else {
        disk_states[raid_name] = disk_states[raid_name] ", " $3 ": " $NF
    }

    next

}

END {

    raid_states[raid_name] = raid_status

    for(name in raid_states) {
        tags["raid"] = name
        tags["diskid-states"] = disk_states[name]
        writeDoubleMetric("raid-status", tags, "gauge", 0, raid_states[name])
    }
    if (overall_raid_status_found) {
        if (missing_disk > 0) {
            raid_configured = 0
        } else {
            raid_configured = 1
        }
        writeDoubleMetric("panw-raid-configured", tags, "gauge", 0, raid_configured )
    }
}
