#! META
name: panos-show-interface-rate
description: Fetch interface information and stats
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
network-interface-rx-rate-mbps:
    why: |
        Generally, tracking the amount of receive data through the interfaces is important to identify any capacity related issues, and apply QoS mechanisms.
    how: |
        This script logs into the Palo Alto Networks firewall through XML API and retrieves the data rate of all physical network interfaces. In that output, it looks for the actual data rate of each interface.
    without-indeni: |
        TThe traffic statistics of network interfaces can be manually reviewed through the CLI.
    can-with-snmp: true
    can-with-syslog: false
network-interface-tx-rate-mbps:
    why: |
        Generally, tracking the amount of transmit data through the interfaces is important to identify any capacity related issues, and apply QoS mechanisms.
    how: |
        This script logs into the Palo Alto Networks firewall through XML API and retrieves the data rate of all physical network interfaces. In that output, it looks for the actual data rate of each interface.
    without-indeni: |
        TThe traffic statistics of network interfaces can be manually reviewed through the CLI.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><interface>hardware</interface></show>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result
_dynamic_vars:
    _dynamic:
        nic:
            _text: "${root}/hw/entry[contains(name, '/')]/name"

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><counter><rate>${nic}</rate></counter></show>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result
_metrics:
    -
        _tags:
            "im.name":
                _constant: "network-interface-rx-rate-mbps"
        _transform:
            _tags:
                "name": |
                    {
                        print dynamic("nic")
                    }
        _value.double:
            _text: ${root}/hw/entry/port/rx-bytes
    -
        _tags:
            "im.name":
                _constant: "network-interface-tx-rate-mbps"
        _transform:
            _tags:
                "name": |
                    {
                        print dynamic("nic")
                    }
        _value.double:
                _text: ${root}/hw/entry/port/tx-bytes