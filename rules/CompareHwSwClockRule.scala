package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class CompareHwSwClockRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_hw_sw_clock_diff", "All Devices: Hardware and software clock mismatch",
    "indeni will alert when a hardware and software clock are different on a device.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("hw-clock-difference").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("hw-clock-difference"), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            GreaterThanOrEqual(
              actualValue,
              ConstantExpression(Some(10.0)))

            // The Alert Item to add for this specific item
      ).withRootInfo(
          getHeadline(),
          scopableStringFormatExpression("The hardware and software clock differ by %.0f seconds. This situation may result in weird issues.", actualValue),
            ConditionalRemediationSteps("Read http://www.tldp.org/LDP/sag/html/hw-sw-clocks.html on why this is important.",
              ConditionalRemediationSteps.VENDOR_CP -> "https://forums.checkpoint.com/forums/message.jspa?messageID=50368"
          )
      ).asCondition()
    ).withoutInfo()
  }
}
