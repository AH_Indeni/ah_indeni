package com.indeni.server.rules.library

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.expressions.conditions.LesserThan
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class SoftwareEosRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Expiration Threshold",
    "How long before end of support should Indeni alert.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(90))

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_software_eos", "All Devices: Software end of support nearing",
    "Indeni will alert a significant time before the software running on a device reaches end of support.", AlertSeverity.ERROR).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("software-eos-date").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set("software-eos-date"), denseOnly = false),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        LesserThan(
          actualValue,
          PlusExpression[Double](NowExpression(), getParameterTimeSpanForTimeSeries(highThresholdParameter)))

        // The Alert Item to add for this specific item
      ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression("The end of support for the software on this device is on %s.", doubleToDateExpression(actualValue)),
        ConditionalRemediationSteps("Upgrade the software to a more recent release.",
          ConditionalRemediationSteps.VENDOR_CP -> "The full information on Check Point's software and hardware end of support is available at: <a target=\"_blank\" href=\"https://www.checkpoint.com/support-services/support-life-cycle-policy/\">Support Life Cycle Policy</a>",
          ConditionalRemediationSteps.VENDOR_PANOS -> "Review Palo Alto Networks support site for full information regarding software end of life: <a target=\"_blank\" href=\"https://www.paloaltonetworks.com/services/support/end-of-life-announcements/end-of-life-summary\">End-of-Life Summary</a>",
          ConditionalRemediationSteps.VENDOR_JUNIPER ->
            """|1. Run "show version"  command to review the current software version.
               |2. Ensure the current software version is supported by Juniper.
               |3. Review Juniper support site for full information regarding software end of life: <a target=\"_blank\" href=\"https://www.juniper.net/support/eol/#software\">End of Life Products & Milestones</a>
               |4. Contact Juniper Networks Technical Assistance Center (JTAC) if further assistance is required.""".stripMargin)
      ).asCondition()
    ).withoutInfo()
  }
}
