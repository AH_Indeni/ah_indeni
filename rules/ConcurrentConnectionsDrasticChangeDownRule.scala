package com.indeni.server.rules.library
/*

  * Rule is cancelled at the moment since we rewrote Change detection algorithm
  * Rule should be re written with the new algorithm


import com.indeni.apidata.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.ResultsFound
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.scope.ScopableExpression
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.tools.changePointAnalysis.DropDetectionExpression
import com.indeni.server.params._
import com.indeni.server.rules._
import com.indeni.server.rules.library.ConcurrentConnectionsDrasticChangeDownRule._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.apidata.time.TimeSpan
*/
/**
  * Rule is cancelled at the moment since we rewrote Change detection algorithm
  * Rule should be re written with the new algorithm

case class ConcurrentConnectionsDrasticChangeDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val reviewedTimeframeParameter = new ParameterDefinition(REVIEWED_TIMEFRAME_PARAMETER_NAME,
    "",
    "Reviewed Timeframe",
    "The window of time we review to detect changes.",
    ParameterDefinition.UIType.TIMESPAN,
    TimeSpan.fromMinutes(120))


  override val metadata: RuleMetadata = RuleMetadata.builder(NAME, "All Devices: Drastic drop in concurrent connections", "Alert when a drop was detected in the number of concurrent connections.", AlertSeverity.ERROR).configParameter(reviewedTimeframeParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val numOfConnectionsTimeSeries = TimeSeriesExpression[Double]("concurrent-connections")
    val actualValue = numOfConnectionsTimeSeries.last
    val lastChange = DropDetectionExpression(numOfConnectionsTimeSeries).withLazy

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("concurrent-connections"), historyLength = getParameterTimeSpanForRule(reviewedTimeframeParameter)),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          ResultsFound(lastChange)

          // The Alert Item to add for this specific item
      ).withRootInfo(
            getHeadline(),
            scopableStringFormatExpression("A drop has been detected in the number of concurrent connections. %s.", new ScopableExpression[Any] {

              override protected def evalWithScope(time: Long, scope: Scope): String = s"Changed from ${lastChange.eval(time).head.before} to ${lastChange.eval(time).head.after}"
              override def args: Set[Expression[_]] = Set(lastChange)
            }),
            ConstantExpression("Review why this may be happening.")
        ).asCondition()
    ).withoutInfo()
  }
}

object ConcurrentConnectionsDrasticChangeDownRule {

  /* --- Constants --- */

  private[library] val NAME = "concurrent_connections_drastic_change_down"

  private[library] val REVIEWED_TIMEFRAME_PARAMETER_NAME: String = "reviewed_timeframe"
}
  */
