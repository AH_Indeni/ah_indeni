package com.indeni.server.rules.library.paloalto

import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.server.common.data.conditions.{Equals, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.RuleHelper
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

class DataplanePoolUsageRule(context: RuleContext, poolName: String, usageThreshold: Double) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Pool_usage"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Pool Usage",
    "What is the threshold for the license usage for which once it is crossed an alert will be issued.",
    UIType.DOUBLE,
    usageThreshold)

  override val metadata: RuleMetadata = RuleMetadata.builder("panw_pool_usage_" + poolName.replaceAll("[^A-Za-z0-9]", ""), "Palo Alto Networks Firewalls: " + poolName + " dataplane pool utilization high",
    "The dataplane of a Palo Alto Networks firewall has several pools, each with a different role. indeni will alert when a pool is near exhaustion.", AlertSeverity.ERROR).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("dataplane-pool-used").last
    val limitValue = TimeSeriesExpression[Double]("dataplane-pool-limit").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("dataplane-pool-used", "dataplane-pool-limit"), condition = Equals("name", poolName), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          GreaterThanOrEqual(
            inUseValue,
            TimesExpression[Double](limitValue, DivExpression[Double](getParameterDouble(highThresholdParameter), ConstantExpression[Option[Double]](Some(100.0)))))

        ).withRootInfo(
            getHeadline(),
            scopableStringFormatExpression("There are %.0f elements used from this pool where the limit is %.0f. This may impact firewall performance.", inUseValue, limitValue),
            ConstantExpression("Contact Palo Alto Networks technical support.")
        ).asCondition()
    ).withoutInfo()
  }
}
