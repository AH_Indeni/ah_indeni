package com.indeni.server.rules.library

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.ts.TimeSinceLastValueExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class OspfNeighborIsDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "High_Threshold_of_Downtime"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Downtime",
    "If a peer device is down or not communicating for at least this amount of time, an alert will be issued.",
    UIType.TIMESPAN,
    TimeSpan.fromMinutes(15))

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_ospf_neighbor_down", "All Devices: OSPF neighbor(s) down",
    "Indeni will alert if one or more OSPF neighbors isn't communicating well.", AlertSeverity.CRITICAL).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val threshold = getParameterTimeSpanForRule(highThresholdParameter).noneable

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("ospf-state")),

        StatusTreeExpression(
          SelectTimeSeriesExpression[Double](context.tsDao, Set("ospf-state"), ConstantExpression(TimeSpan.fromDays(1)), denseOnly = false),
          GreaterThan(TimeSinceLastValueExpression(TimeSeriesExpression("ospf-state"), Some(1.0)), threshold)
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")}"),
          EMPTY_STRING,
          title = "Neighbors Affected"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more OSPF neighbors are down."),
      ConditionalRemediationSteps("Review the cause for the neighbors being down.",
        ConditionalRemediationSteps.VENDOR_CP -> "Review sk84520: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk84520",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Review https://live.paloaltonetworks.com/t5/Configuration-Articles/OSPF-Adjacencies-are-Established-but-the-OSPF-Routes-are-Not/ta-p/58328 . You can also log into the device over SSH and run \"less mp-log routed.log\".",
        ConditionalRemediationSteps.OS_NXOS ->
          """This issue might be caused by:
            | a. L2/L3 connectivity issue
            | b. OSPF not being enabled on the interface
            | c. an interface is defined as passive
            | d. a mismatched subnet mask
            | e. a mismatched hello/dead interval
            | f. a mismatched authentication key
            | g. a mismatched area ID
            | i. a mismatched transit/stub/Not-So-Stubby Area (NSSA) option
            |Check OSPF configuration:
            |Use these commands in order to check the OSPF configuration (subnet, hello/dead interval, area ID, area type, authentication key (if any), and not-passive), and ensure that it matches on both sides.
            | a. "show run ospf"
            | b. "show ip ospf PID interface"
            | c. "show ip ospf PID"
            |
            |Troubleshooting OSPF States:
            |1. OSPF Neighbor Stuck in the Initialization (INIT) State. This issue might be caused by:
            | a. one side is blocking the hello packet with ACL
            | b. one side is translating, with Network Address Translation (NAT), the OSPF hello
            | c. the multicast capability of one side is broken (L2)
            |
            |2. OSPF Neighbor Stuck in a Two-Way State. This issue might be caused by OSPF priority set equal to zero.
            |NOTE: It is normal in broadcast network types.
            |
            |3. OSPF Neighbor Stuck in Exstart/Exchange. This issue might be caused by:
            | a. MTU mismatch
            | b. Neighbor Router ID (RID) is the same as its neighbor's
            | c. ACL blocking unicast - after two-way OSPF sends unicast packet
            |
            |4. OSPF Neighbor Stuck in a Loading State. This issue might be caused by bad Link State Advertisement (LSA). Check the OSPF-4-BADLSATYPE message with the "show ip ospf bad" and the "show log" commands.
            |
            |Further details can be found in: <a target="_blank" href="https://www.cisco.com/c/en/us/support/docs/switches/nexus-7000-series-switches/116422-technote-ospf-00.html"> CISCO Nexus OSPF troubleshooting guide|</a>.""".stripMargin,
        ConditionalRemediationSteps.VENDOR_JUNIPER ->
          """|1. Run the "show ospf neighbor" command to view the current state of OSPF neighbors.
             |2. Run the "show configuration protocol ospf" command to view OSPF configuration.
             |3. If no OSPF neighbor is shown, please check the following items:
             |  a. physical and data link connectivity
             |  b. mismatched IP subnet
             |  c. subnet mask
             |  d. area number
             |  e. area type
             |  f. authentication
             |  g. hello and dead interval
             |  i. network type
             |4. It's normal for DR-other neighbors to be stuck in two-way state.
             |5. If the OSPF neighbor is stuck in Exstart/Exchange State, check mismatched IP MTU.
             |6. Consider configuring traceoptions to track the OSPF operations.
             |7. Review the following articles on Juniper TechLibrary for more information:
             | a. <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/topic-map/ospf-traceoptions.html">Example: Configuring OSPF Trace Options</a>
             | b. <a target="_blank" href="https://kb.juniper.net/InfoCenter/index?page=content&id=KB14881">OSPF is in '2 Way' state with some neighbors and "Full" with others</a>
             |8. If the problem persists, contact the Juniper Networks Technical Assistance Center (JTAC).""".stripMargin
      )
    )
  }
}


