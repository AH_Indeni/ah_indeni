package com.indeni.server.rules.library.crossvendor

import com.indeni.ruleengine.expressions.conditions.{And, GreaterThan, LesserThan}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, _}
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.library.{ConditionalRemediationSteps, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.server.common.data.conditions.True
import com.indeni.apidata.time.TimeSpan


case class CrossVendorContractHasExpiredRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "Effective_Duration_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Effective Duration Threshold",
    "How many days the contract expiration alert should be effective.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(30))

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_contract_has_expired", "All Devices: Contract(s) has expired",
    "Indeni will alert when a contract has expired. " +
      "Contracts that have expired more that a set number of days will be ignored. " +
      "The threshold for the number of days after contract expiration can be adjusted by the user.", AlertSeverity.WARN).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("contract-expiration").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("contract-expiration")),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("contract-expiration"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            LesserThan(
              actualValue,
              NowExpression()
            ),
            GreaterThan(
              PlusExpression[Double](actualValue, getParameterTimeSpanForTimeSeries(highThresholdParameter)),
              NowExpression()
            )
          )

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")}"),
          scopableStringFormatExpression("Expired on %s", doubleToDateExpression(actualValue)),
          title = "Affected Contracts"
        ).asCondition()
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more contracts has expired. See the list below."),
      ConditionalRemediationSteps("Renew any contracts that need to be renewed.",
        ConditionalRemediationSteps.VENDOR_CP ->
          """Make sure you have purchased the required contracts and have updated them in your management server. Review:
            |<a target="_blank" href="https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk33089">Solution sk33089 on Check Point Support Center</a>.""".stripMargin,
        ConditionalRemediationSteps.VENDOR_PANOS ->
          """Review this article on Palo Alto Networks Support Site:
            |<a target="_blank" href="https://www.paloaltonetworks.com/documentation/80/pan-os/pan-os/getting-started/activate-licenses-and-subscriptions">Activate Licenses and Subscriptions</a>.""".stripMargin
      )
    )
  }
}
