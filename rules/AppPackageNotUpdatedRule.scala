package com.indeni.server.rules.library

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.expressions.conditions.GreaterThan
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class AppPackageNotUpdatedRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "AppPackageNotUpdatedThreshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "App Package Not Updated Threshold",
    "If time since last app package update has elapsed more than this threshold, Indeni will alert.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(30))

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_app_package_not_updated", "All Devices: App package needs update",
    "Indeni will alert when app package has not been updated.", AlertSeverity.WARN).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("package-last-update").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("package-last-update")),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("package-last-update"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          GreaterThan(
              NowExpression(),
              PlusExpression[Double](actualValue, getParameterTimeSpanForTimeSeries(highThresholdParameter)))

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")}"),
          scopableStringFormatExpression("Last update on %s", doubleToDateExpression(actualValue)),
          title = "Affected Packages"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more app packages need update. See the list below."),
      ConditionalRemediationSteps("Renew any app packages that need to be updated.",
        ConditionalRemediationSteps.VENDOR_FORTINET ->
          """
            |1. Login via ssh to the Fortinet firewall and execute the FortiOS “get system fortiguard-service status” command to list current update package versions and license expiry date and status. 
            |2. Login via ssh to the Fortinet firewall and execute the FortiOS “diag autoupdate versions” and “diag autoupdate status” commands to get more details about the update policy and last update of the UTM services. 
            |3. Login via https to the Fortinet firewall and go to the menu System > Dashboard > Status to locate the License Information widget. All subscribed services should have a green checkmark, indicating that connections are successful. A gray X indicates that the FortiGate unit cannot connect to the FortiGuard network, or that the FortiGate unit is not registered. A red X indicates that the FortiGate unit was able to connect but that a subscription has expired or has not been activated. 
            |4. Verify that the Fortigate has internet access and then login via https to the Fortinet firewall to view the FortiGuard connection status by going to System > Config > FortiGuard menu. Select “Update Now” under AV & IPS Download Options to force a sync. In case of an update problem with the Web Filtering and Email Filtering expand the “Web Filtering and Email Filtering Options” and Change Port Selection to use an Alternate Port (8888) and press “Test Availability”. Wait a few minutes and verify the license health status. 
            |5. If the registration does not appear after changing to Alternate Port mentioned to the above step, try pinging the FortiGuard services URL using command “exec ping service.fortiguard.net”. If that resolves to an IP then type the following commands:
            | - diag debug app update -1
            | - diag debug en
            | - exec update-now
            |If it does not resolve to an IP then this is a DNS issue.
            |Note: At the end disable the debug.
            |6. If you are using multiple VDOMS on the FortiGate, make sure that you have an Internet-facing VDOM set as the management vdom.
            |7. For more information about licensing review  the next  online article “Setting up FortiGuard services” : http://cookbook.fortinet.com/setting-fortiguard-services-54/
            |8. Contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin
      )
    )
  }
}
