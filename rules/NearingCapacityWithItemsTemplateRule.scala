package com.indeni.server.rules.library

import com.indeni.ruleengine.Scope.{Scope, ScopeValueHelper}
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.{And, ConditionHelper, GreaterThanOrEqual, Or, ResultsFound}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.math.{DivExpression, TimesExpression}
import com.indeni.ruleengine.expressions.regex.RegexExpression
import com.indeni.ruleengine.expressions.scope.{ScopableExpression, ScopeValueExpression}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules.library.ThresholdDirection.ThresholdDirection
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.util.matching.Regex

/**
  * Similar to NearingCapacityTemplateRule, except it supports having specific items in the alert.
  */
class NearingCapacityWithItemsTemplateRule(context: RuleContext,
                                           ruleName: String,
                                           ruleFriendlyName: String,
                                           ruleDescription: String,
                                           severity: AlertSeverity = AlertSeverity.ERROR,
                                           usageMetricName: String,
                                           limitMetricName: String = null,
                                           threshold: Double,
                                           metaCondition: TagsStoreCondition = True,
                                           applicableMetricTag: String,
                                           alertItemDescriptionFormat: String,
                                           alertDescription: String,
                                           baseRemediationText: String,
                                           thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE,
                                           minimumValueToAlert: Double = 0,
                                           alertItemsHeader: String,
                                           itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""),
                                           itemsToIgnore: Set[Regex] = Set("^$".r))
                                          (vendorToRemediationText: (String, String)*)
  extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Store_use"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Usage",
    "indeni will evaluate the current utilization vs the limit and alert if the percentage of usage crosses this number.",
    UIType.DOUBLE,
    threshold)

  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, severity).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {

    val inUseValue = TimeSeriesExpression[Double](usageMetricName).last
    val limitValue = if (null != limitMetricName) TimeSeriesExpression[Double](limitMetricName).last else ConstantExpression(Some(100.0))

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set(applicableMetricTag), withTagsCondition(usageMetricName, limitMetricName)),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, if (null != limitMetricName) Set(usageMetricName, limitMetricName) else Set(usageMetricName), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            generateCompareCondition(
              thresholdDirection,
              inUseValue,
              TimesExpression[Double](limitValue, DivExpression[Double](getParameterDouble(highThresholdParameter), ConstantExpression[Option[Double]](Some(100.0))))),
            GreaterThanOrEqual(inUseValue, ConstantExpression(Some(minimumValueToAlert))),
            Or(
              itemsToIgnore.map(r => ResultsFound(RegexExpression(ScopeValueExpression(applicableMetricTag).visible().asString(), r))).toSeq
            ).not)

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"),
          new ScopableExpression[String] {
            val innerDescription = scopableStringFormatExpression(alertItemDescriptionFormat, inUseValue, limitValue)

            override protected def evalWithScope(time: Long, scope: Scope): String = {
              val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
              innerDescription.eval(time) + itemSpecificDescription.collectFirst {
                case item if (!item._1.findFirstMatchIn(metricTagValue).isEmpty) => item._2
              }.get
            }

            override def args: Set[Expression[_]] = Set(innerDescription)
          },
          title = alertItemsHeader
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}
