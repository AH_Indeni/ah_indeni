package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.conditions.{And, Equals, Not}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.language.reflectiveCalls

case class CrossVendorSpanningTreeRootChangeRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_spanning_tree_root_change", "Switching Devices: Spanning tree root node has changed",
    "When spanning tree is enabled, each switch device keep track of a spanning tree root node. A change of the spanning tree root node can be caused by incorrect " +
      "root election priority setup or unexpected link state change or device state change. If spanning tree root node changes, Indeni will alert", AlertSeverity.WARN).build()

  override def expressionTree: StatusTreeExpression = {
    val currentValue = SnapshotExpression("spanning-tree-root").asSingle().mostRecent().noneable
    val previousValue = SnapshotExpression("spanning-tree-root").asSingle().middle()

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set("spanning-tree-root")).single(),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        And(
          previousValue.nonEmpty,
          Not(Equals(currentValue, previousValue)))
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("The spanning tree root node has changed."),
        ConditionalRemediationSteps("Review the cause for the spanning tree root node change.",
           ConditionalRemediationSteps.OS_NXOS -> """1.	Execute “show spanning-tree” and “show spanning-tree summary” to identify the STP root for all the configured vlans.
                                                    |2. Run “show spanning-tree vlan X detail” to collect more info about the STP topology (X=vlanid).
                                                    |3.	Check the event history to find where the Topology Change Notifications originate from by running “show spanning-tree internal event-history tree X brief” , (X=vlanid).
                                                    |4.	Display the STP events of an interface with “show spanning-tree internal event-history tree Y interface X brief” , (X=vlanid, Y=interfaceid).
                                                    |5.	Consider to hard code the STP root and backup root to the core switches by configuring a lower STP priority.
                                                    |6.	Activate the recommended vPC “peer switch” NX-OS command to a pure peer switch topology in which the devices all belong to the vPC.
                                                    |7.	Consider to use Root Guard feature to enforce the root bridge placement in the network. If a received BPDU triggers an STP convergence that makes that designated port become a root port, that port is put into a root-inconsistent (blocked) state.
                                                    |8.	For more information please review  the folloing article: <a target="_blank" href="https://www.cisco.com/c/en/us/support/docs/switches/nexus-5000-series-switches/116199-technote-stp-00.html">STP troubleshooting guide</a>""".stripMargin,

           ConditionalRemediationSteps.VENDOR_JUNIPER -> "For more information review the following article: <a target=\"_blank\" href=\"https://www.juniper.net/documentation/en_US/junos/topics/concept/layer-2-services-stp-guidelines-statement-bridge-priority.html\">Bridge Priority for Election of Root Bridge and Designated Bridge</a>"
        )
    )
  }
}
