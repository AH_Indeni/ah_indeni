package com.indeni.server.rules.library

import com.indeni.ruleengine.InvisibleScopeKey
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.conditions.Equals
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity



abstract class BaseVpnTunnelIsDownRule(context: RuleContext,
                                       ruleName: String,
                                       ruleFriendlyName: String = "Firewall Devices: Dynamic VPN tunnel(s) down",
                                       alertTags: Set[String],
                                       secondaryInfoHeadline: String) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName,
    "Indeni will alert if one or more dynamic VPN tunnels is down.", AlertSeverity.INFO).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("vpn-tunnel-state").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, alertTags, True),

        // The condition which, if true, we have an issue. Checked against the metrics we've collected
        StatusTreeExpression(
          SelectTimeSeriesExpression[Double](context.tsDao, Set("vpn-tunnel-state"), denseOnly = false),
          Equals(actualValue, ConstantExpression(Some(0.0)))
        ).withSecondaryInfo(
          scopableStringFormatExpression(secondaryInfoHeadline),
          scopableStringFormatExpression("This tunnel is down"),
          title = "Dynamic VPN Tunnels Affected"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more dynamic VPN tunnels are down."),
      ConditionalRemediationSteps("Under normal operation status, non permenant VPN tunnels may go up and down dynamically due to traffic activity.",
        ConditionalRemediationSteps.VENDOR_CP -> "Indeni uses the \"vpn tu\" command on the firewall to determine gateway status. Open SmartView Tracker and look for recent logs pertaining to the VPN peers listed above. Consider reading: <a target=\"_blank\" href=\"https://indeni.com/check-point-firewalls-troubleshoot-a-vpn-connection/\">How to Troubleshoot Check Point Firewall VPN Connection</a>",
        ConditionalRemediationSteps.VENDOR_PANOS -> "Review <a target=\"_blank\" href=\"https://live.paloaltonetworks.com/t5/Management-Articles/How-to-Troubleshoot-IPSec-VPN-connectivity-issues/ta-p/59187\">How to Troubleshoot IPSec VPN connectivity issues</a>",
        ConditionalRemediationSteps.VENDOR_JUNIPER ->
          """|Run "show security ipsec inactive-tunnels" command to review inactive tunnels and tunnel down reasons.
             |2. Run "show security ipsec security-associations brief [detail]" to check if Phase 1 and Phase 2 are up.
             |3. Check the Phase 1 and Phase 2 configuration. Ensure they are matched on both ends.
             |4. Check if any filtering is applied to access-list, policy or NAT.
             |5. Check the routes to the remote peer.
             |6. Check the logs for VPN tunnel reporting
             |7. Consider enabling VPN monitoring for the tunnel status.
             |8. Consider setting traceoptions by running "set security ike traceoptions file vpn.tr size 5m files 5 world-readable" command for more detailed information.
             |9. Review the following articles on Juniper tech support site: <a target="_blank" href="https://kb.juniper.net/InfoCenter/index?page=content&id=KB10100&actp=METADATA">How to troubleshoot a VPN tunnel that is down or not active</a>
             |9. <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/concept/secuity-vpn-monitoring-understanding.html">Understanding VPN Monitoring</a>
             |10. Contact Juniper Networks Technical Assistance Center (JTAC) if further assistance is required.""".stripMargin
      )
    )
  }
}

case class VpnTunnelIsDownNonVsxRule(context: RuleContext) extends BaseVpnTunnelIsDownRule(context,
  ruleName = "cross_vendor_vpn_tunnel_down_nonvsx",
  alertTags = Set("peerip", "name"),
  secondaryInfoHeadline = "${scope(\"name\")} (${scope(\"peerip\")})"
)

case class VpnTunnelIsDownVsxRule(context: RuleContext) extends BaseVpnTunnelIsDownRule(context,
  ruleName = "cross_vendor_vpn_tunnel_down_vsx",
  alertTags = Set("peerip", "name", "vs.id", "vs.name"),
  secondaryInfoHeadline = "${scope(\"name\")} (${scope(\"peerip\")}) (${scope(\"vs.id\")}) (${scope(\"vs.name\")})"
)
