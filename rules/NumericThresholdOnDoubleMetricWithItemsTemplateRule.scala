package com.indeni.server.rules.library

import com.indeni.ruleengine.Scope.{Scope, ScopeValueHelper}
import com.indeni.ruleengine.expressions.Expression
import com.indeni.ruleengine.expressions.casting.ToDoubleExpression
import com.indeni.ruleengine.expressions.conditions.{And, ConditionHelper, Or, ResultsFound}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.DivExpression
import com.indeni.ruleengine.expressions.regex.RegexExpression
import com.indeni.ruleengine.expressions.scope.{ScopableExpression, ScopeValueExpression}
import com.indeni.server.common.data.TagsStoreDao
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.ThresholdDirection.ThresholdDirection
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.util.matching.Regex

class NumericThresholdWithItemsTemplateRule(context: RuleContext,
                                            ruleName: String,
                                            ruleFriendlyName: String,
                                            ruleDescription: String,
                                            severity: AlertSeverity = AlertSeverity.ERROR,
                                            value: Expression[Option[Double]],
                                            tagsDao: TagsStoreDao,
                                            selectExpression: SelectExpression[Scope],
                                            metricName: String,
                                            threshold: Any,
                                            metaCondition: TagsStoreCondition = True,
                                            applicableMetricTag: String,
                                            alertItemDescriptionFormat: String,
                                            alertDescription: String,
                                            unitConverter: ConstantExpression[Option[Double]] = ConstantExpression[Option[Double]](Some(1.0)),
                                            baseRemediationText: String,
                                            thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE,
                                            alertItemsHeader: String,
                                            itemsToIgnore: Set[Regex] = Set("^$".r),
                                            itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""))
                                           (vendorToRemediationText: (String, String)*) extends PerDeviceRule with RuleHelper {

  private[library] val thresholdParameterName = "threshold"
  private val thresholdParameter = new ParameterDefinition(thresholdParameterName,
    "",
    "Alerting Threshold",
    "Indeni will alert if the value is " + (if (thresholdDirection == ThresholdDirection.ABOVE) "above" else "below") + " this value.",
    UIType.fromObjectClass(threshold.getClass),
    threshold)

  override val metadata: RuleMetadata = RuleMetadata.builder(ruleName, ruleFriendlyName, ruleDescription, AlertSeverity.ERROR).configParameter(thresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val thresholdValue = getParameter(thresholdParameter)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      StatusTreeExpression(
        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(tagsDao, Set(applicableMetricTag), withTagsCondition(metricName)),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          selectExpression,

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            generateCompareCondition(
              thresholdDirection,
              value,
              thresholdValue),
            Or(
              itemsToIgnore.map(r => ResultsFound(RegexExpression(ScopeValueExpression(applicableMetricTag).visible().asString(), r))).toSeq
            ).not)

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}"),
          new ScopableExpression[String] {
            val innerDescription = scopableStringFormatExpression(alertItemDescriptionFormat, DivExpression(value, unitConverter))

            override protected def evalWithScope(time: Long, scope: Scope): String = {
              val metricTagValue = scope.getVisible(applicableMetricTag).get.toString
              innerDescription.eval(time) + itemSpecificDescription.collectFirst {
                case item if (!item._1.findFirstMatchIn(metricTagValue).isEmpty) => item._2
              }.get
            }

            override def args: Set[Expression[_]] = Set(innerDescription)
          },
          title = alertItemsHeader
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(alertDescription),
      ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
    )
  }
}


class NumericThresholdOnDoubleMetricWithItemsTemplateRule(context: RuleContext,
                                                          ruleName: String,
                                                          ruleFriendlyName: String,
                                                          ruleDescription: String,
                                                          severity: AlertSeverity = AlertSeverity.ERROR,
                                                          metricName: String,
                                                          threshold: Any,
                                                          metaCondition: TagsStoreCondition = True,
                                                          applicableMetricTag: String,
                                                          alertItemDescriptionFormat: String,
                                                          alertDescription: String,
                                                          unitConverter: ConstantExpression[Option[Double]] = ConstantExpression[Option[Double]](Some(1.0)),
                                                          baseRemediationText: String,
                                                          thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE,
                                                          alertItemsHeader: String, itemsToIgnore: Set[Regex] = Set("^$".r),
                                                          itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""))
                                                         (vendorToRemediationText: (String, String)*)
  extends NumericThresholdWithItemsTemplateRule(
    context,
    ruleName,
    ruleFriendlyName,
    ruleDescription,
    severity,
    TimeSeriesExpression[Double](metricName).last,
    context.tsDao,
    SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),
    metricName,
    threshold,
    metaCondition,
    applicableMetricTag,
    alertItemDescriptionFormat,
    alertDescription,
    unitConverter,
    baseRemediationText,
    thresholdDirection,
    alertItemsHeader,
    itemsToIgnore,
    itemSpecificDescription)(vendorToRemediationText: _*) {
}

class NumericThresholdOnComplexMetricWithItemsTemplateRule(context: RuleContext,
                                                           ruleName: String,
                                                           ruleFriendlyName: String,
                                                           ruleDescription: String,
                                                           severity: AlertSeverity = AlertSeverity.ERROR,
                                                           metricName: String,
                                                           threshold: Any,
                                                           metaCondition: TagsStoreCondition = True,
                                                           applicableMetricTag: String,
                                                           alertItemDescriptionFormat: String,
                                                           alertDescription: String,
                                                           unitConverter: ConstantExpression[Option[Double]] = ConstantExpression[Option[Double]](Some(1.0)),
                                                           baseRemediationText: String,
                                                           thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE,
                                                           alertItemsHeader: String,
                                                           itemsToIgnore: Set[Regex] = Set("^$".r),
                                                           itemSpecificDescription: Seq[(Regex, String)] = Seq(".*".r -> ""))
                                                          (vendorToRemediationText: (String, String)*) extends NumericThresholdWithItemsTemplateRule(
  context, ruleName, ruleFriendlyName, ruleDescription, severity,

  ToDoubleExpression(SingleSnapshotExtractExpression(SnapshotExpression(metricName).asSingle().mostRecent().value(), "value")), context.snapshotsDao, SelectSnapshotsExpression(context.snapshotsDao, Set(metricName)).single(),
  metricName, threshold, metaCondition, applicableMetricTag, alertItemDescriptionFormat, alertDescription, unitConverter, baseRemediationText, thresholdDirection, alertItemsHeader, itemsToIgnore, itemSpecificDescription)(vendorToRemediationText: _*) {

}
