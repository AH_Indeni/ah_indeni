package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class CrossVendorCpuSoftLockup(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "Cross_Vendor_cpu_soft_lockup",
  ruleFriendlyName = "All Devices: CPU Soft Lockup-test2",
  ruleDescription = "Many devices that rely on Linux kernels can experience CPU soft lockups. It can be critical to identify them early enough before all the cores are seized.",
  metricName = "cores-locked",
  alertIfDown = false,
  alertDescription = "The following device had core(s) experience lockup. Please be aware that if all cores lockup, the device may fail. Indeni tracks if the cores have lockedup in the last 10 minutes",
  baseRemediationText = "Please review the process that is causing the lockup for the core. Indeni will autoresolve the alert if the cores are unlocked.")(
  ConditionalRemediationSteps.VENDOR_CP ->
    """|
       |1. Please examine the processes responsible for the CPU soft lockups.
       |2. To do so, you will need to run the following command: "/var/log/messages* | grep "soft lockup"
       |3. There are several Secure Knowledge articles such as the following: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk97772. If you cannot find the associate SK article with the responsible process, please reach out to Check Point Support.""".stripMargin
)
