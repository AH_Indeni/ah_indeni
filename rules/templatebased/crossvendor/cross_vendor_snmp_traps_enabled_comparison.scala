package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  *
  */
case class CrossVendorSnmpTrapsEnabledComparison(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_snmp_traps_enabled_comparison",
  ruleFriendlyName = "Clustered Devices: SNMP traps enabled settings do not match across cluster members",
  severity = AlertSeverity.INFO,
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the SNMP settings do not match.",
  metricName = "snmp-traps-status",
  isArray = true,
  alertDescription = "Devices that are part of a cluster should have the same SNMP configuration. Review the differences below.",
  baseRemediationText = "Ensure all of the SNMP settings are configured correctly on all cluster members.")(
  ConditionalRemediationSteps.VENDOR_JUNIPER ->
    """|1. On the device command line interface execute "show configuration snmp" command to review SNMP configuration.
       |2. For security reasons, it is highly recommended to use SNMP version 3.
       |3. Check if the community string is not set to "public".  This is commonly used as a default for SNMP community string and presents a security vulnerability.
       |4. Review the following article on Juniper TechLibrary for more information: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/task/configuration/snmpv3-community-configuring-junos-nm.html">Configuring the SNMPv3 Community</a>.""".stripMargin
)
