package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}

/**
  *
  */
case class cross_vendor_compare_domain(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_domain",
  ruleFriendlyName = "Clustered Devices: Cluster members' domain names mismatch",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the domain setting is different.",
  metricName = "domain",
  isArray = false,
  alertDescription = "The members of a cluster of devices must have the same domain settings.",
  baseRemediationText = "Review the settings of each device in the cluster and ensure they are the same.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Use the "ip domain-name" NX-OS command to configure the same domain name for the clustered devices
      |2. Use the vrf context command in case that is needed to enter to the VRF context mode to configure the domain  for a particular VRF.
      |3. For more information please review  the following CISCO Configuration guide:
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/command/reference/sysmgmt/n5k-sysmgmt-cr/n5k-sm_cmds_i.html#pgfId-1659356
    """.stripMargin
)
