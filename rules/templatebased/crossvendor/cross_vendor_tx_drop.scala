package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class CrossVendorTxDrop(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_tx_drop",
  ruleFriendlyName = "All Devices: TX packets dropped",
  ruleDescription = "Indeni tracks the number of packets that had issues and alerts if the ratio is too high.",
  usageMetricName = "network-interface-tx-dropped",
  limitMetricName = "network-interface-tx-packets",
  applicableMetricTag = "name",
  threshold = 0.5,
  minimumValueToAlert = 100.0, // We don't want to alert if the number of packets is really low
  alertDescription = "Some network interfaces and ports are experiencing a high drop rate. Review the ports below.",
  alertItemDescriptionFormat = "%.0f dropped packets identified out of a total of %.0f transmitted.",
  baseRemediationText = "Packet drops usually occur when the rate of packets transmitted is higher than the device ability to handle.",
  alertItemsHeader = "Affected Ports")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
       |1. Run the "show interface" command to review the interface counters and the bitrate. Consider to configure the "load-interval 30" interface sub command to improve the accuracy of the interface measurements. Check for traffic bursts and high traffic utilization.
       |2. Use the "show hardware rate-limit" NX-OS command (if supported) to determine if packets are being dropped because of a rate limit.
       |3. Execute the "show policy-map interface control-plane" NX-OS command to determine if packets are being dropped because of a QoS policy.
       |4. Use the "show hardware internal statistics rates" to determine if packets are being dropped by the hardware.
       |5. Run the "show hardware internal statistics pktflow all" NX-OS command to display per ASIC statistics, including packets into and out of the ASIC. This command helps to identify where packet loss is occurring.
    """.stripMargin,
  ConditionalRemediationSteps.VENDOR_JUNIPER ->
    """|1. Run the “show interface extensive” command to review the interface statistics.
       |2. Check for packet drops and input/output traffic rate.
       |3. Run the “show class-of-service interface x/x/x detail"  to determine any QoS policy applied to interface which may cause packet drops.
       |4. If the interface is saturated, the number of packets dropped by the is indicated by the input queue of the I/O Manager ASIC. This number increments once for every packet that is dropped by the ASIC's RED mechanism.
       |5. Review the following article on Juniper tech support site: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/concept/using-show-commands-for-packet-drops.html">Understanding Dropped Packets and Untransmitted Traffic Using show Commands</a>
       |6. If the problem persists, contact the Juniper Networks Technical Assistance Center (JTAC)""".stripMargin,
  ConditionalRemediationSteps.VENDOR_FORTINET ->
    """
       |1. Run "diag hardware deviceinfo nic <interface>" command to display a list of hardware related error names and values. Review  the next link for more details: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-toubleshooting-54/troubleshooting_tools.htm
       |2. Run command "fnsysctl cat /proc/net/dev" to get a summary of the interface statistics.
       |3. Check for speed and duplex mismatch in the interface settings on both sides of a cable, and check for a damaged cable. Review the next link for more info: http://kb.fortinet.com/kb/documentLink.do?externalID=10653""".stripMargin
)
