package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_network_port_duplex_half(context: RuleContext) extends SingleSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_network_port_duplex_half",
  ruleFriendlyName = "All Devices: Network port(s) running in half duplex",
  ruleDescription = "Indeni will alert one or more network ports is running in half duplex.",
  metricName = "network-interface-duplex",
  applicableMetricTag = "name",
  alertItemsHeader = "Ports Affected",
  alertDescription = "One or more ports are set to half duplex. This is usually an error. Review the list of ports below.",
  baseRemediationText = "Many times ports are in half duplex due to an autonegotation error or a misconfiguration.",
  complexCondition = RuleEquals(RuleHelper.createComplexStringConstantExpression("half"), SnapshotExpression("network-interface-duplex").asSingle().mostRecent().value().noneable))(
  ConditionalRemediationSteps.VENDOR_CP -> "Review sk83760: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk83760",
  ConditionalRemediationSteps.VENDOR_PANOS -> "https://live.paloaltonetworks.com/t5/Management-Articles/How-to-Display-Port-Information-Connected-Media-Interface/ta-p/61715",
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Use the "show interface" NX-OS command to display speed and duplex settings of an interface.
      |2. Use the "show interface status" and "show interface capabilities" NX-OS commands to gather more information about ports.
      |3. You can disable link negotiation using the "no negotiate auto" command. Use the "negotiate auto" command to enable auto negotiation on 1-Gigabit ports when the connected peer does not support auto negotiation. By default, auto-negotiation is enabled on 1-Gigabit ports and disabled on 10-Gigabit ports.
      |4. Cisco does not recommend to enable auto negotiation on 10-Gigabit ports. Enabling auto-negotiation on 10-Gigabit ports brings the link down. By default, link negotiation is disabled on 10-Gigabit ports.
      |NOTE: A shut and no shut to the interface may be required after the aforementioned configuration change.""".stripMargin,
  ConditionalRemediationSteps.VENDOR_FORTINET ->
    """|
      |1. Monitor hardware network operations and speed by using the "diag hardware deviceinfo nic <interface>" FortiOS command.
      |2. Run the command "diag hardware deviceinfo nic <interface>" command to display a list of hardware related names and values. Review the next link for more details: http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-toubleshooting-54/troubleshooting_tools.htm
      |3. Run the hidden FortiOS command "fnsysctl cat /proc/net/dev" to get a summary of the interface statistics.
      |4. Check for a mismatch in the speed and duplex interface settings on both sides of a cable, or for a damaged cable. Try to manually configure both sides to the same mode when you can. Review the next link "Symptoms of Ethernet speed/duplex mismatches" for more info: http://kb.fortinet.com/kb/documentLink.do?externalID=10653""".stripMargin
)
