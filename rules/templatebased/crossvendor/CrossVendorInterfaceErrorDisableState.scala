package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.ruleengine.expressions.core._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class CrossVendorInterfaceErrorDisableState(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "CrossVendorInterfaceErrorDisableState",
  ruleFriendlyName = "All Devices: Interface(s) in error-disable state",
  ruleDescription = "Interfaces can be put in err-disable state if certain errors are detected by the devices. Indeni will alert if this happens.",
  metricName = "network-interface-err-disable",
  applicableMetricTag = "name",
  alertIfDown = false,
  alertItemsHeader = "Affected Interfaces",
  alertDescription = "Interfaces can be put in err-disable state if certain errors are detected by the devices.\nThis includes:\n* Flapping links\n* Spanning Tree BPDUs detected with BPDU Guard enabled\n* Detected a physical loop\n* Uni-directional link detected by UDLD",
  baseRemediationText = "Review the causes why some interfaces were put in error-disable state."
)(ConditionalRemediationSteps.OS_NXOS ->
  """|1. Use the "show interface" and "show interface status err-disabled" NX-OS commands to identify the reason for the err-disable interface state.
     |2. View information about the internal state transitions of the port by using the "show system internal ethpm event-history interface X/X" NX-OS command.
     |3. Review the logs for relevant findings by running the "show logging" NX-OS command.
     |4. After fixing the issue, run the "shut/no shut" command on the port to re-enable it.
     |5. It is possible to enable automatic periodic err-disable recovery by using the "errdisable recovery cause" NX-OS configuration command. For more information please review  the following CISCO  NX-OS guide:
     |<a target="_blank" href="https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/layer2/421_n1_1/b_Cisco_n5k_layer2_config_gd_rel_421_n1_1/Cisco_n5k_layer2_config_gd_rel_421_n1_1_chapter3.html#task_3B5CB60B4E8746FA900E16679C66B437">Enabling the Error-Disabled Detection</a>.""".stripMargin,
  ConditionalRemediationSteps.VENDOR_JUNIPER ->
    """|1. On the device command line interface execute the "show ethernet-switching interfaces" command to review the status of interfaces.
       |2. Check if the MAC limit or MAC move limit is not exceeded.
       |3. Check if the storm control is in effect.
       |4. Review whether multiple devices are connected to the port.
       |5. Check if the device is not moving too frequently or rate-limiting is not exceeded.
       |6. Review the following article on Juniper tech support site: <a target="_blank" href="https://www.juniper.net/documentation/en_US/junos/topics/reference/command-summary/show-ethernet-switching-interfaces-qfx-series.html">Ethernet Switching Feature Guide: show ethernet-switching interfaces</a>.""".stripMargin
)
