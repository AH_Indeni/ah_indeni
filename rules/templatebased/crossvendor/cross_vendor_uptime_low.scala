package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.expressions.core.ConstantExpression
import com.indeni.server.common.data.conditions.Equals
import com.indeni.server.params.ParameterDefinition.ParameterUnit
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class cross_vendor_uptime_low(context: RuleContext) extends NumericThresholdOnDoubleMetricTemplateRule(context,
  ruleName = "cross_vendor_uptime_low",
  ruleFriendlyName = "All Devices (Non-VSX): Device restarted (uptime low)",
  ruleDescription = "Indeni will alert when a device has restarted.",
  severity = AlertSeverity.CRITICAL,
  metricName = "uptime-milliseconds",
  threshold = Threshold(TimeSpan.fromMinutes(60), ParameterUnit.MILLISECONDS),
  thresholdDirection = ThresholdDirection.BELOW,
  alertDescriptionFormat = "The current uptime is %.0f seconds which seems to indicate the device has restarted.",
  unitConverter = ConstantExpression[Option[Double]](Some(1000.0)),
  baseRemediationText = "Determine why the device was restarted.",
  metaCondition = !Equals("vsx", "true"))(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
       |1. Use the "show version" or "show system reset-reason" NX-OS commands to display the reason for the reload.
       |2. Use the "show cores" command to determine if a core file was recorded during the unexpected reboot.
       |3. Run the "show process log" command to display the processes and if a core was created.
       |4. With the show logging command, review the events that happened close to the time of reboot.""".stripMargin,
  ConditionalRemediationSteps.VENDOR_FORTINET ->
    """
      |1. Watch the system reboot time.
      |2. Review the log messages and focus on error messages that were generated at least 5 minutes prior to system reboot, especially before unexpected system reboot.
      |3. Verify the status of the scheduled restart command to making  sure it's an irregular restart
      |   - config sys global
      |   - get | grep restart
      |   - end
      |4. Login via ssh to the Fortinet firewall and review the crash log in a readable format by using the FortiOS command “diag debug crashlog read”.
      |5. Contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin
)
