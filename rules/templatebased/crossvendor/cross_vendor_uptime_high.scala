package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.ruleengine.expressions.core.ConstantExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NumericThresholdOnDoubleMetricTemplateRule, Threshold, ThresholdDirection}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.apidata.time.TimeSpan
import com.indeni.server.params.ParameterDefinition.ParameterUnit

/**
  *
  */
case class cross_vendor_uptime_high(context: RuleContext) extends NumericThresholdOnDoubleMetricTemplateRule(context,
  ruleName = "cross_vendor_uptime_high",
  ruleFriendlyName = "All Devices: Device uptime too high",
  ruleDescription = "Indeni will alert when a device's uptime is too high",
  severity = AlertSeverity.ERROR,
  metricName = "uptime-milliseconds",
  threshold = Threshold(TimeSpan.fromDays(365 * 10), ParameterUnit.MILLISECONDS),
  thresholdDirection = ThresholdDirection.ABOVE,
  alertDescriptionFormat = "The current uptime is %.0f seconds. This alert identifies when a device has been up for a very long time and may need an upgrade.",
  unitConverter = ConstantExpression[Option[Double]](Some(1000.0)),
  baseRemediationText = "Upgrade the device. You may also change the alert's threshold, or disable the alert completely, if not needed.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
       |1. Use the "show version" NX-OS command to display the current system uptime.
       |2. Run the "show system reset-reason" to check the reason for the last reboot of the device.
       |3. Check if the installed NX-OS version is supported and review it for software bugs.""".stripMargin
)
