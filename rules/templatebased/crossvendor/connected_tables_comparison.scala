package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}
import com.indeni.server.common.data.conditions.{Equals => DataEquals}

/**
  *
  */
case class connected_tables_comparison_vsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "connected_tables_comparison_vsx",
  ruleFriendlyName = "Clustered Devices: Connected networks do not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the networks they are directly connected to do not match.",
  metricName = "connected-networks-table",
  applicableMetricTag = "vs.name",
  metaCondition = DataEquals("vsx", "true"),
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same directly connected networks. Review the differences below.",
  baseRemediationText = "Ensure all of the required ports are configured correctly on all cluster members, including the subnet mask.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Ensure all of the required interfaces are configured accordingly on all cluster members.
      |2. Consider to suspending this alert in case of orphan ports configured to one of the vPC peer switches.""".stripMargin
)

case class connected_tables_comparison_nonvsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "connected_tables_comparison_nonvsx",
  ruleFriendlyName = "Clustered Devices: Connected networks do not match across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the networks they are directly connected to do not match.",
  metricName = "connected-networks-table",
  metaCondition = !DataEquals("vsx", "true"),
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same directly connected networks. Review the differences below.",
  baseRemediationText = "Ensure all of the required ports are configured correctly on all cluster members, including the subnet mask.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
       |1. Ensure all of the required interfaces are configured accordingly on all cluster members.
       |2. Consider to suspending this alert in case of orphan ports configured to one of the vPC peer switches.""".stripMargin
)
