package com.indeni.server.rules.library.templatebased.fortinet

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NumericThresholdOnDoubleMetricWithItemsTemplateRule

case class FortinetLogDiskUsage(context: RuleContext) extends NumericThresholdOnDoubleMetricWithItemsTemplateRule(context,
    ruleName = "FortinetLogDiskUsage",
    ruleFriendlyName = "Fortinet Devices: Log disk utilization is high",
    ruleDescription = "Indeni will alert when the log disk utilization on Fortinet devices is high.",
    metricName = "fortios-hd-usage",
    threshold = 80.0,
    applicableMetricTag = "name",
    alertItemsHeader = "Affected Disks",
    alertItemDescriptionFormat = "The current disk log usage is %.0f%%",
    alertDescription = "Log disk utilization is high.",
    baseRemediationText =
       """1. Login via https to the Fortinet firewall and then go to the menu "Log & Report" to review the Local Disk utilization pie and Historical Disk Usage graph.
         |2. Login via ssh to the Fortinet firewall and run the FortiOS command “diagnose sys logdisk usage” to review the HD usage and the HD logging space per VDOM.
         |3. If the disk is almost full, transfer the logs or data off the disk to free up space. When a disk is almost full it consumes a lot of resources to find the free space and organize the files. Clean all unused files routinely.
         |4. Remove any debug files after debugging is done.
         |5. If the FortiGate unit has a hard disk, it is enabled by default to store logs. Consider storing logs to Syslog, FortiAnalyzer or FortiCloud instead of memory or hard disk. Logging to local disk will impact overall performance and reduce the lifetime of the unit. Fortinet recommends logging to feature rich FortiCloud or FortiAnalyzer which don’t use much CPU resources.
         |6. Consider enabling the email alert FortiOS feature if the disk usage exceeds 75%. To achieve this login via https to the Fortinet firewall and then go to the menu “Log & Report” to enable the Email Alert Settings. Then choose the “Disk usage exceeds” tab. More details can be found at http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-system-administration-54/Monitoring/Alert%20email.htm
         |7. If the FortiGate unit has only flash memory, disk logging is disabled by default, and it is recommended to keep this default setting. Constant rewrites to flash drives can reduce the lifetime and efficiency of the memory. 
         |8. Both logging and WAN Optimization can use hard disk space to save data. On the FortiGate, go to System > Advanced > Disk Settings to switch between Local Log and WAN Optimization. More details can be found at http://help.fortinet.com/fos50hlp/54/Content/FortiOS/fortigate-WAN-opt-54/wan_op_intro.htm
         |9. Contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin
    )()
