package com.indeni.server.rules.library.templatebased.bluecoat.proxysg

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class BluecoatHealthMonitorRule(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "BluecoatHealthMonitorRule",
  ruleFriendlyName = "Bluecoat Devices: Critial conditions detected by health monitors",
  ruleDescription = "Bluecoat devices use health monitors to detect critical conditions. Indeni will alert if health monitors detect a critical condition.",
  metricName = "bluecoat-health-monitor",
  applicableMetricTag = "monitor-name",
  descriptionMetricTag = "monitor-state",
  alertItemsHeader = "Critical Condition Detected by Health Monitors",
  alertDescription = "A critical condition is detected by bluecoat health monitor.",
  baseRemediationText = "Troubleshoot the cause of this reported critical condition") ()
