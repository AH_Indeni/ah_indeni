package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

case class CiscoNexusFexStatusPortForward(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_nexus_fex_status_port_forward",
  ruleFriendlyName = "Cisco Nexus: Nexus FEX Diagnostic ASIC forwarding port status failed",
  ruleDescription = "Indeni will alert if Nexus FEX ASIC forwarding ports has failed",
  metricName = "fex-diagnostic-status-port-forward",
  applicableMetricTag = "name",
  alertItemsHeader = "Alert items",
  alertDescription = "Online diagnostic which checks the forwarding ports on the FEX ASIC (application-specific integrated circuit) has failed",
  baseRemediationText = "Display the results of the diagnostic tests for a Fabric Extender chassis by running the “show diagnostic result fex all” command. Review the output to identify the result of the forwarding (server) ports on the ASIC (application-specific integrated circuit) test. Run the \"show fex detail\" command and review the output. Investigate the relevant logs triggered by the FEX by running the “show logging” command. Review the next online document https://www.cisco.com/c/en/us/support/docs/switches/nexus-2000-series-fabric-extenders/200265-Troubleshooting-Fabric-Extender-FEX-Pe.html. Finally, contact CISCO TAC for further assistance."
)()
