package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
//// ----->>>> This Rule was disable per IKP-1686, we will need to add it back once we will fix the IND data collection
case class check_point_signature_update_status(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "check_point_signature_update_status",
  ruleFriendlyName = "Check Point Firewalls: Signature update status",
  ruleDescription = "Indeni has detected that one or more signature databases for software blades are out of date. " +
    "This will happen either if the gateway reports a failure to update, or if the signature version are more than 14 days old. This means that protection against new threats is affected. ",
  metricName = "signature-update-status",
  applicableMetricTag = "blade",
  alertIfDown = true,
  alertItemsHeader = "Affected blades",
  alertDescription = "Software blade signatures are out of data.\n\n" +
    "This alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/johnathanbrowall\">Johnathan Browall Nordstrom</a>.",
  baseRemediationText = "Contact support to get help in troubleshooting this issue. " +
    "There are also troubleshooting guides that can help to determine why the updates has failed.\n" +
    "Anti-bot and Anti-virus: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk98665\n" +
    "URL Filter: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk35196")()
  */
