package com.indeni.server.rules.library.templatebased.paloaltonetworks

import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.paloalto._

/**
  *
  */
case class panw_dataplane_regex_results(context: RuleContext) extends DataplanePoolUsageRule(context, "Regex Results", 80.0)
