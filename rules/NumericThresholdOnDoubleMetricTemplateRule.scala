package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.ruleengine.expressions.math.DivExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.{ParameterUnit, UIType}
import com.indeni.server.rules._
import com.indeni.server.rules.library.ThresholdDirection.ThresholdDirection
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

class NumericThresholdOnDoubleMetricTemplateRule(context: RuleContext,
                                                 ruleName: String,
                                                 ruleFriendlyName: String,
                                                 ruleDescription: String,
                                                 severity: AlertSeverity = AlertSeverity.ERROR,
                                                 metricName: String,
                                                 threshold: Threshold,
                                                 metaCondition: TagsStoreCondition = True,
                                                 alertDescriptionFormat: String,
                                                 unitConverter: ConstantExpression[Option[Double]] = ConstantExpression[Option[Double]](Some(1.0)),
                                                 baseRemediationText: String,
                                                 thresholdDirection: ThresholdDirection = ThresholdDirection.ABOVE
                                                )
                                                (vendorToRemediationText: (String, String)*) extends PerDeviceRule with RuleHelper {

  private[library] val thresholdParameterName = "threshold"
  private val thresholdParameter =
    new ParameterDefinition(thresholdParameterName,
      "",
      "Alerting Threshold",
      "Indeni will alert if the value is " + (if (thresholdDirection == ThresholdDirection.ABOVE) "above" else "below") + " this value.",
      UIType.fromObjectClass(threshold.value.getClass),
      threshold.value,
      threshold.parameterUnit)

  override val metadata: RuleMetadata = RuleMetadata
    .builder(ruleName, ruleFriendlyName, ruleDescription, AlertSeverity.ERROR)
    .configParameter(thresholdParameter)
    .build()

  override def expressionTree: StatusTreeExpression = {

    // This is the value used to evaluate this rule
    val value = TimeSeriesExpression[Double](metricName).last

    // This is the threshold value for the rule condition
    val thresholdValue = getParameter(thresholdParameter)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectTimeSeriesExpression[Double](context.tsDao, Set(metricName), denseOnly = false),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        generateCompareCondition(
          thresholdDirection,
          value,
          thresholdValue)

        // The Alert Item to add for this specific item
      ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression(alertDescriptionFormat, DivExpression(value, unitConverter)),
        ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText: _*)
      ).asCondition()
    ).withoutInfo()
  }
}
