package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.OptionalExpression
import com.indeni.ruleengine.expressions.core.{ConstantExpression, StatusTreeExpression}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression}
import com.indeni.ruleengine.expressions.tools.OddOneOutExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class OddDnsResponseTimeRule(context: RuleContext) extends PerDeviceRule with RuleHelper {
  private[library] val percentageDistanceParameterName = "Relative_Distance_Threshold"
  private val percentageDistanceParameter = new ParameterDefinition(percentageDistanceParameterName,
    "",
    "Relative Distance (%)",
    "If the DNS server response time Relative Distance to the baseline in percentage is bigger than the Relative Distance, and the DNS server response time absolute distance to baseline is bigger than the value set in " +
      "Absolute Distance (ms), an alert will be issued..",
    UIType.DOUBLE,
    0.9)


  private[library] val absoluteDistanceParameterName = "Absolute_Distance_Threshold"
  private val absoluteDistanceParameter = new ParameterDefinition(absoluteDistanceParameterName,
    "",
    "Absolute Distance (ms)",
    "If the DNS server response time Absolute Distance from the baseline and the DNS server response time Relative Distance in percentage is bigger than the value set in " +
      "\"" + percentageDistanceParameter.getFriendlyName + "\"" + ", an alert will be issued..",
    UIType.DOUBLE,
    100)


  override val metadata: RuleMetadata = RuleMetadata.builder(
    "cross_vendor_odd_dns_server_response_time",
    "All Devices: DNS server response time is slow compared to other DNS servers",
    "indeni will alert when a DNS server takes longer to respond than other DNS servers.",
    AlertSeverity.ERROR).configParameters(absoluteDistanceParameter, percentageDistanceParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val absoluteDistance: OptionalExpression[Double] = getParameterDouble(absoluteDistanceParameter)
    val percentageDistance: OptionalExpression[Double] = getParameterDouble(percentageDistanceParameter)
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),
      StatusTreeExpression(
        // The scopes of the time-series we check the test condition against:
        OddOneOutExpression(absoluteDistance,
          percentageDistance,
          alertExceededLowValues = false,
          SelectTimeSeriesExpression[Double](context.tsDao, Set("dns-response-time"))),
        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        OddOneOutExpression.isTimeSeriesOdd("dns-response-time")

        // The Alert Item to add for this specific item
      ).withSecondaryInfo(
        scopableStringFormatExpression("${scope(\"TS:dns-response-time>dns-server\")}"),
        scopableStringFormatExpression("This server's response time is above normal"),
        title = "Affected Servers"
      )
        .asCondition()
      // Details of the alert itself
    ).withRootInfo(
      getHeadline(),
      ConstantExpression(
        "One or more DNS servers have a high response time compared to other DNS servers. This may slow down critical system processes."),
      ConstantExpression("Identify any network and server issues which may be causing this.")
    )
  }
}
