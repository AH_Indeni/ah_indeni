package com.indeni.server.rules.library

import com.indeni.ruleengine.expressions.conditions.Change
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.common.data.conditions
import com.indeni.server.common.data.conditions.{TagsStoreCondition, True}
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.server.common.data.conditions.{Equals => DataEquals}

class ClusterMemberNoLongerActiveTemplateRule(context: RuleContext,
                                              ruleName: String,
                                              metricName: String,
                                              metaCondition: TagsStoreCondition = conditions.True)
  extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata
    .builder(ruleName,
      "Clustered Devices: Cluster member no longer Active",
      "Indeni will alert if the cluster member used to be Active and is now Standby or down.",
      AlertSeverity.ERROR)
    .build()

  override def expressionTree: StatusTreeExpression = {
    val stateTs = TimeSeriesExpression[Double]("cluster-member-active")

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set(metricName), withTagsCondition("cluster-member-active")),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("cluster-member-active"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          Change(
            stateTs,
            ConstantExpression(Some(1.0)),
            ConstantExpression(Some(0.0)))

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"" + metricName + "\")}"),
          EMPTY_STRING,
          title = "Clustering Mechanisms Affected"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("This cluster member used to be Active. It no longer is. Failover may have occurred"),
      ConditionalRemediationSteps("Review the possible cause and troubleshoot.",
        ConditionalRemediationSteps.OS_NXOS ->
          """|1. Verify the communication between the FHRP peers. A random, momentary loss of data communication between the peers is the most common problem that results in continuous FHRP state change (ACT<-> STB) unless this error message occurs during the initial installation.
             |2. Check the CPU utilization by using the "show process CPU" NX-OS command. HSRP state changes are often due to High CPU Utilization.
             |3. Common problems for the loss of FHRP packets between the peers to investigate are physical layer problems, excessive network traffic caused by spanning tree issues or excessive traffic caused by each Vlan.
             |
             |A vPC problem could cause the change to the state so check the next :
             |1. Check that STP bridge assurance is not enabled on the vPC links. Bridge assurance should only be enabled on the vPC peer link.
             |2. Compare the vPC domain IDs of the two switches and ensure that they match. Execute the "show vpc brief"  to compare the output that should match across the vPC peer switches.
             |3. Verify that both the source and destination IP addresses used for the peer-keepalive messages are reachable from the VRF associated with the vPC peer-keepalive link. Then, execute the "sh vpc peer-keepalive" NX-OS command and review the output from both switches.
             |4. Verify that the peer-keepalive link is up. Otherwise, the vPC peer link will not come up.
             |5. Review the vPC peer link configuration and execute the "sh vpc brief" NX-OS command and review the output. Besides, verify that the vPC peer link is configured as a Layer 2 port channel trunk that allows only vPC VLANs.
             |6. Ensure that type 1 consistency parameters match. If they do not match, then vPC is suspended. Items that are type 2 do not have to match on both Nexus switches for the vPC to be operational. Execute the "sh vpc consistency-parameters" command and review the output.
             |7. Verify that the vPC number that you assigned to the port channel that connects to the downstream device from the vPC peer device is identical on both vPC peer devices.
             |8. If you manually configured the system priority, verify that you assigned the same priority value on both vPC peer devices.
             |9. Verify that the primary vPC is the primary STP root and the secondary vPC is the secondary STP root.
             |10. Review the logs for relevant findings.
             |11. For more information please review: <a target="_blank" href="https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/troubleshooting/guide/N5K_Troubleshooting_Guide/n5K_ts_vpc.html">vPC Troubleshooting Guide</a>.""".stripMargin,
        ConditionalRemediationSteps.VENDOR_JUNIPER ->
          """|1. On the device command line interface execute "show chassis cluster status"  command to review the cluster status.
             |2. Run "show chassis cluster interface" to check if control and fabric links are configured correctly and up.
             |3. Check if the priority of each node if configured correctly.
             |4. Review the following article on Juniper TechLibrary for more information: <a target="_blank" href="https://kb.juniper.net/InfoCenter/index?page=content&id=KB20987&actp=METADATA">[SRX] Troubleshooting Chassis Cluster Redundancy Group not failing over</a>.""".stripMargin
      )
    )
  }
}

case class ClusterMemberNoLongerActiveNonVsxRule(context: RuleContext)
  extends ClusterMemberNoLongerActiveTemplateRule(context,
    "cross_vendor_cluster_member_no_longer_active_non_vsx",
    "name")

case class ClusterMemberNoLongerActiveVsxRule(context: RuleContext)
  extends ClusterMemberNoLongerActiveTemplateRule(context,
    "cross_vendor_cluster_member_no_longer_active_vsx",
    "vs.name",
    DataEquals("vsx", "true"))
