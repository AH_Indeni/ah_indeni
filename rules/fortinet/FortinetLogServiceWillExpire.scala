package com.indeni.server.rules.library.fortinet

import com.indeni.apidata.time.TimeSpan
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThan, LesserThan}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.PlusExpression
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.common.data.conditions.True
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.rules.library.{ConditionalRemediationSteps, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity


case class FortinetLogServiceWillExpire(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private val highThresholdParameterName = "ahead_alerting_threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Expiration Threshold",
    "How long before log service expiration should Indeni alert.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(30))

  override val metadata: RuleMetadata = RuleMetadata.builder("FortinetLogServiceWillExpire", "Fortinet Devices: Log service expiration nearing",
    "Indeni will alert when Fortinet log service is about to expire.", AlertSeverity.WARN).configParameter(highThresholdParameter).build()

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("fortios-log-service-expire-date").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(

        // The additional tags we care about (we'll be including this in alert data)
        SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("fortios-log-service-expire-date")),

        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("fortios-log-service-expire-date"), denseOnly = false),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          And(
            GreaterThan(
              actualValue,
              NowExpression()
            ),
            LesserThan(
              actualValue,
              PlusExpression[Double](NowExpression(), getParameterTimeSpanForTimeSeries(highThresholdParameter))))

          // The Alert Item to add for this specific item
        ).withSecondaryInfo(
          scopableStringFormatExpression("${scope(\"name\")}"),
          scopableStringFormatExpression("Will expire on %s", doubleToDateExpression(actualValue)),
          title = "Affected Log Services"
        ).asCondition()
      ).withoutInfo().asCondition()
    ).withRootInfo(
      getHeadline(),
      ConstantExpression("One or more log services are about to expire. See the list below."),
      ConditionalRemediationSteps("Renew any log services that need to be renewed.",
        ConditionalRemediationSteps.VENDOR_FORTINET ->
          """
            |1. Login via ssh to the Fortinet firewall and execute the FortiOS "get system fortiguard-service status" and "diag autoupdate versions"
              |>>> commands to list current update package versions and license expiry status.
            |2. Login via https to the Fortinet firewall and go to the menu System > Dashboard > Status to locate the License Information widget.
              |>>> All subscribed services should have a green checkmark, indicating that connections are successful. A gray X indicates that the
              |>>> FortiGate unit cannot connect to the FortiGuard network, or that the FortiGate unit is not registered. A red X indicates that
              |>>> the FortiGate unit was able to connect but that a subscription has expired or has not been activated.
            |3. Login via https to the Fortinet firewall to view the FortiGuard connection status by going to System > Config > FortiGuard menu.
            |4. Purchase additional licenses if needed.
            |5. Consider enabling the alert email setting to the Fortinet firewall in order to receive an alert email prior to FortiGuard license
              |>>> expiration (notification date range: 1 - 100 days). The current alert email status can be provided with the next command:
              |>>> "get alertemail setting". More details can be found at: https://docs.fortinet.com/uploaded/files/2798/fortigate-cli-ref-54.pdf
            |6. For more information about licensing review  the next  online article "Setting up FortiGuard services" :
              |>>> http://cookbook.fortinet.com/setting-fortiguard-services-54/
            |7. If the problem persists, contact Fortinet Technical support at https://support.fortinet.com/ for further assistance.""".stripMargin.replaceAll("\n>>>", "")
      )
    )
  }
}
