package com.indeni.server.rules.library

import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.server.common.data.conditions.True
import com.indeni.server.rules._
import com.indeni.server.rules.library.core.PerDeviceRule
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

/**
  * This rule allows ind scripts to pull in their own interesting logs, separate from Dendron.
  * This is usually done for log infromation that cannot be sent via syslog, or is best analyzed
  * over SSH by using grep/awk or something similar.
  */
case class InterestingLogsRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata.builder("cross_vendor_interesting_logs", "All Devices: Interesting logs found",
    "For each supported device, indeni will look for logs that are deemed \"interesting\" and alert when these are found.", AlertSeverity.ERROR).build()

  override def expressionTree: StatusTreeExpression = {
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        SelectSnapshotsExpression(context.snapshotsDao, Set("interesting-logs")).multi(),
        StatusTreeExpression(
          IterateSnapshotDimensionExpression("interesting-logs"),
          com.indeni.ruleengine.expressions.conditions.True
        ).withSecondaryInfo(
            scopableStringFormatExpression("${scope(\"interesting-logs:line\")}"),
            scopableStringFormatExpression("${scope(\"interesting-logs:info\")}"),
            title = "Interesting Logs"
        ).asCondition()
      ).withoutInfo().asCondition()
      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("Interesting logs have been found on this device. Review the list below for which logs were found and what action should be taken."),
        ConstantExpression("For each line, extended information is included with specific remediation steps.")
    )

  }
}


